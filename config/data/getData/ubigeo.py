import json

from django.db import transaction, IntegrityError
from apps.security.models import ubigeo

def ubi():
    try:
        with transaction.atomic():
            if len(ubigeo.objects.all()) == 0:
                with open('json/ubigeo.json') as f:
                    col = json.load(f)

                for i in col:
                    c = ubigeo(**i)
                    c.save()
                    print("se agrego el ubigeo " + c.region)

            else:
                raise ValueError("Hay ubigeos ingresados, limpia la base de datos")

    except IntegrityError as e:
        error = str(e)
        print(error)
'''
try:
    with transaction.atomic():
        if len(ubigeo.objects.all()) == 0:
            with open('../csv/ubigeo.csv') as csvfile:
                reader = csv.DictReader(csvfile, delimiter=';')
                cc = []
                for row in reader:
                    cc.append(row)
                with open('ubigeo.json', 'w') as col_file:
                    json.dump(cc, col_file)
                for row in reader:
                    c = ubigeo(**row)
                    co = c.save()
                    print("se agrego el ubigeo " + c.region)

        else:
            raise ValueError("Hay ubigeos ingresados, limpia la base de datos")

except IntegrityError as e:
    error = str(e)
    print(error)

'''