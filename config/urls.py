from django.conf.urls import url, include
from django.contrib import admin
from django.conf.urls.static import static
from django.conf import settings

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^', include('apps.security.urls')),
    url(r'^', include('apps.reportes.urls')),
    url(r'^', include('apps.test_psyco.urls')),
  url(r'^', include('apps.adm_institucion.urls')),
  url(r'^', include('apps.adm_test.urls')),
  url(r'^', include('apps.adm_assistance.urls')),
url(r'^', include('apps.adm_security.urls')),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
