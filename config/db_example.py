DATABASES = {
    'default': {
       'ENGINE'  : 'django_tenants.postgresql_backend',
       'NAME'    : 'testcore',
       'USER'    : 'postgres',
       'PASSWORD': '123',
       'HOST'    : 'localhost',
       'PORT'    : '5433',
    }
}

#schemas
#DATABASE_ROUTERS = (
#    'django_tenants.routers.TenantSyncRouter',
#)

#TENANT_MODEL = "security.Client" # app.Model
#TENANT_DOMAIN_MODEL = "security.Domain" # app.Model

SESSION_ENGINE = "django.contrib.sessions.backends.cache"
SESSION_CACHE_ALIAS = "default"
DJANGO_REDIS_IGNORE_EXCEPTIONS = True
DJANGO_REDIS_LOG_IGNORED_EXCEPTIONS = True


#redis
CACHES = {
    "default": {
        "BACKEND": "django_redis.cache.RedisCache",
        "LOCATION": "redis://127.0.0.1:6379/1",
        "OPTIONS": {
            "CLIENT_CLASS": "django_redis.client.DefaultClient",
            #'KEY_FUNCTION': 'django_tenants.cache.make_key',
            #'REVERSE_KEY_FUNCTION': 'django_tenants.cache.reverse_key',
            "SOCKET_CONNECT_TIMEOUT": 5,  # in seconds
            "SOCKET_TIMEOUT": 5,  # in seconds
            "COMPRESSOR": "django_redis.compressors.zlib.ZlibCompressor",
            "IGNORE_EXCEPTIONS": True,
        }
    }
}

BROKER_URL = 'redis://localhost:6379/1'
