import os
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

SECRET_KEY = 'ngens^vfcp@n8pnc)a*abz*_^jf$ua4mpd&$q1m98d2y5347j2'

# Application definition

THIRD_PARTY_APPS = (
    'registration',
    'storages',
    #'sslserver',
    #'djcelery'
)


SHARED_APPS = (
    #'django_tenants',  # mandatory
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',

) + THIRD_PARTY_APPS


TENANT_APPS = (
    # The following Django contrib apps must be in TENANT_APPS
    'django.contrib.contenttypes',
    'apps.security',
    #'apps.psicologia',
    'apps.test_psyco',
    # 'apps.apafa',
    # 'apps.capturaMac'
    # 'apps.psicologia.test_psyco',
    # 'apps.psicologia.programacion_test',
    'apps.adm_institucion',
    'apps.reportes',
    'apps.adm_test',
    'apps.adm_assistance',
    'apps.adm_security',

)

INSTALLED_APPS = list(SHARED_APPS) + [app for app in TENANT_APPS if app not in SHARED_APPS]

#INSTALLED_APPS = DJANGO_APPS + THIRD_PARTY_APPS + LOCAL_APPS


MIDDLEWARE  = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'apps.security.core.MiddlewareFunciones.MiddelwareValidarInstitucion'  #validar Institución
]

ROOT_URLCONF = 'config.urls'


TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.contrib.auth.context_processors.auth',
                'django.template.context_processors.debug',
                'django.template.context_processors.i18n',
                'django.template.context_processors.media', #para los archivos media
                'django.template.context_processors.static',
                'django.template.context_processors.tz',
                'django.contrib.messages.context_processors.messages',
                'django.template.context_processors.request',
            ],
        },
    },
]


WSGI_APPLICATION = 'config.wsgi.application'


# Password validation
# https://docs.djangoproject.com/en/1.9/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]


# Internationalization
# https://docs.djangoproject.com/en/1.9/topics/i18n/

LANGUAGE_CODE = 'es-pe'

TIME_ZONE = 'America/Lima'

USE_I18N = True

USE_L10N = True

USE_TZ = False


AUTH_USER_MODEL = 'adm_security.Users'

LOGIN_REDIRECT_URL = '/'

ACCOUNT_ACTIVATION_DAYS = 7


# Static asset configuration


STATIC_ROOT = 'staticfiles'
STATIC_URL = '/static/'

STATICFILES_DIRS = (
    os.path.join(BASE_DIR, 'static'),
)


MEDIA_ROOT = os.path.join(BASE_DIR, 'static/media')
MEDIA_URL = '/media/'
EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
'''

EMAIL_HOST = "smtp.gmail.com"
EMAIL_HOST_USER = "ventas@perucore.com"
EMAIL_HOST_PASSWORD = ",.-,.-,.-"
EMAIL_PORT = 587
EMAIL_USE_TLS = True
'''

EMAIL_HOST = 'smtp.gmail.com'
EMAIL_HOST_USER = 'perucoresoft@gmail.com'
EMAIL_HOST_PASSWORD = ',.-,.-,.-'
EMAIL_PORT = 587
EMAIL_USE_TLS = True


# import excel, csv, odt
FILE_UPLOAD_HANDLERS = ("django_excel.ExcelMemoryFileUploadHandler",
                        "django_excel.TemporaryExcelFileUploadHandler")