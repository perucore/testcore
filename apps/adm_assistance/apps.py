from django.apps import AppConfig


class AdmAssistanceConfig(AppConfig):
    name = 'adm_assistance'
