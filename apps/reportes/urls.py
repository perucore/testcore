from django.conf.urls import url, include
from .views import repotetiempotest,tablarender,timepregunta,tablapreguntapre,mostrartietes,tablatetes,mostraroobser\
    ,tablaobser,mostravalora,tablavaloracion,graficaobser, valorpregunta

urlpatterns = [
    url(r'^reporte/institucion/mostrartiempotest/', repotetiempotest),
    url(r'^reporte/institucion/tablarender/', tablarender),
    url(r'^reporte/institucion/tiempopregunta/', timepregunta),
    url(r'^reporte/institucion/tablatiempopregunta/', tablapreguntapre),
    url(r'^reporte/institucion/mostrartietes/', mostrartietes),
    url(r'^reporte/institucion/tablatietes/', tablatetes),
    url(r'^reporte/institucion/observaciones/', mostraroobser),
    url(r'^reporte/institucion/tablaobservaciones/', tablaobser),
    url(r'^reporte/institucion/resportevaloracion/', mostravalora),
    url(r'^reporte/institucion/tablavaloracion/', tablavaloracion),
    url(r'^reporte/institucion/graficaobservacion/', graficaobser),

    #marcado por test
    url(r'^reporte/institucion/valorporpregunta/', valorpregunta),

]
