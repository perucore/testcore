from django.apps import AppConfig


class AdmInstitucionConfig(AppConfig):
    name = 'apps.adm_institucion'
