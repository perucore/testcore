from apps.security.core.join_list import validate_dict
from django.contrib.auth.decorators import login_required
from django.shortcuts import HttpResponse

from apps.security.core.crud import ListBase, SaveBase, UpdateBase
from ..forms.f_directives import directivesForm
from ..models import directives


class list_directives(ListBase):
  template_name = 'directives/list_directives.html'
  def l_queryset(self):
     arg = {}
     if self.request.GET:
        arg = validate_dict(self.request.GET)
        arg['status'] = True
        arg['ie_id']  = self.request.session['institucion']['id']
     return directives.objects.filter(**arg).order_by('-id')

class add_directives(SaveBase):
  form_class = directivesForm
  template_name = 'directives/frm_directives.html'

class update_directives(UpdateBase):
  form_class = directivesForm
  template_name = 'directives/frm_directives.html'
  model = directives

@login_required(login_url='login/')
def deletedirectives(em, id):
  dp = directives.objects.get(pk=id)
  dp.status = False
  dp.save()
  return HttpResponse('DATOS ELIMINADOS') 