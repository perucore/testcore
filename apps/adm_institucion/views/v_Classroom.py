from apps.security.core.format_date import format_date
from apps.security.core.join_list import join_list
from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect
from django.shortcuts import HttpResponse

from apps.adm_institucion.models import directives
from apps.security.core.crud import ListBase, SaveBase, UpdateBase, ResponseLoginView
from apps.adm_security.models import Users
from ..forms.f_Classroom import ClassroomForm
from ..models import Classroom


class list_Classroom(ListBase):
  template_name = 'Classroom/list_Classroom.html'

  def l_queryset(self):
    queryset = Classroom.__get__data__ie__(self,{})
    return queryset

class add_Classroom(SaveBase):
  form_class = ClassroomForm
  template_name = 'Classroom/frm_Classroom.html'

  def form_valid(self, form):
    ss = form.save(commit=False)
    ss.institucion_id = int(self.request.session["institucion"]["id"])
    ss.save()
    return HttpResponseRedirect(self.get_success_url())

class update_Classroom(UpdateBase):
  form_class = ClassroomForm
  template_name = 'Classroom/frm_Classroom.html'
  model = Classroom

class alumnosClassroom(list_Classroom):
  template_name = 'Classroom/list_AlumnosClassroom.html'

  def l_queryset(self):
    queryset = Classroom.__get__data__ie__(self,{'id':self.request.GET.get('cr')})
    return queryset


class load_users(ResponseLoginView):

      def response(self, context):
          filehandle = self.request.FILES['file']
          di = dict(filehandle.get_dict(sheet_name=self.request.POST.get('room')))
          j =join_list(di)
          for u in j:
              print(u)
              if u['birth_date']:
                date_b = format_date(str(u['birth_date']))

              di = {
                  'institucion_id'   : self.request.session['institucion']['id'],
                  'dni'              : u["dni"],
                  'names'            : u["names"].lower(),
                  'first_surname'    : u["first_surname"].lower(),
                  'second_surname'   : u["second_surname"].lower(),
                  'sex'              : 'm' if u["sex"]=='Mujer' else 'f',
                  'birth_date'       : date_b,

              }
              lu = Users.objects
              up = lu.filter(dni=u["dni"])
              if "email" in u.keys():
                  if u["email"] is not None:
                      di["email"] = u["email"]
              if "cellphone" in u.keys():
                  if u["cellphone"] is not None:
                      di["cellphone"] = u["cellphone"]

              if up.values():
                  print("yes update")
                  idus = up.values('id')[0]["id"]
                  for ii in up:
                    ii.profiles.add(int(u["profile_id"]))

                  up.update(**di)
                  if self.request.POST.get('rc') is None:
                      dir = directives.objects.filter(directive_id = idus)
                      if dir:
                          dir.update(occupation = u["ocupation"])
                      else:
                          directives(
                              ie_id=self.request.session['institucion']['id'],
                              directive_id=idus,
                              occupation=u["ocupation"]
                          ).save()
              else:
                  print("no update, create")
                  di['username'] = u["dni"]
                  di['password_default'] = u["dni"]
                  uu = lu.create(**di)
                  uu.set_password(u["dni"])
                  uu.save()
                  idus = uu.id
                  uu.profiles.add(int(u["profile_id"]))

                  if self.request.POST.get('rc') is not None:
                      e = Classroom.objects.get(pk=int(self.request.POST.get('rc')))
                      e.student.add(uu.id)
                  else:
                      directives(
                          ie_id = self.request.session['institucion']['id'],
                          directive_id = idus,
                          occupation   = u["ocupation"]
                      ).save()

          return HttpResponse("Guardado Correctamente")



@login_required(login_url='login/')
def deleteClassroom(em, id):
  dp = Classroom.objects.get(pk=id)
  dp.status = False
  dp.save()
  return HttpResponse('DATOS ELIMINADOS') 