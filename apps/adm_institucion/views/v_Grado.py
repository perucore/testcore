from django.contrib.auth.decorators import login_required
from django.shortcuts import HttpResponse

from apps.security.core.crud import ListBase, SaveBase, UpdateBase
from ..forms.f_Grado import GradoForm
from ..models import Grado


class list_Grado(ListBase):
  template_name = 'Grado/list_Grado.html'
  queryset = Grado.objects.filter(status=True)

class add_Grado(SaveBase):
  form_class = GradoForm
  template_name = 'Grado/frm_Grado.html'

class update_Grado(UpdateBase):
  form_class = GradoForm
  template_name = 'Grado/frm_Grado.html'
  model = Grado

@login_required(login_url='login/')
def deleteGrado(em, id):
  dp = Grado.objects.get(pk=id)
  dp.status = False
  dp.save()
  return HttpResponse('DATOS ELIMINADOS') 