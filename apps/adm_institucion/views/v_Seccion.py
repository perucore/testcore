from django.contrib.auth.decorators import login_required
from django.shortcuts import HttpResponse

from apps.security.core.crud import ListBase, SaveBase, UpdateBase
from ..forms.f_Seccion import SeccionForm
from ..models import Seccion


class list_Seccion(ListBase):
  template_name = 'Seccion/list_Seccion.html'
  queryset = Seccion.objects.filter(status=True).values()

class add_Seccion(SaveBase):
  form_class = SeccionForm
  template_name = 'Seccion/frm_Seccion.html'

class update_Seccion(UpdateBase):
  form_class = SeccionForm
  template_name = 'Seccion/frm_Seccion.html'
  model = Seccion

@login_required(login_url='login/')
def deleteSeccion(em, id):
  dp = Seccion.objects.get(pk=id)
  dp.status = False
  dp.save()
  return HttpResponse('DATOS ELIMINADOS') 