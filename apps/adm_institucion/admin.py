from django.contrib import admin
from .models import Institucion, Seccion, Grado, Classroom, NivelEducativo#, Det_Programa_Test, Programa_Test

# Register your models here.
admin.site.register(Institucion)
admin.site.register(Seccion)
admin.site.register(Grado)
admin.site.register(Classroom)
admin.site.register(NivelEducativo)
#admin.site.register(Det_Programa_Test)