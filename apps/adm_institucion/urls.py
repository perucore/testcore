from django.conf.urls import url 
#views.
from .views.view_institucion import View_Institucion
from .views.v_Classroom import list_Classroom,add_Classroom,update_Classroom,deleteClassroom, alumnosClassroom,load_users
from .views.v_Seccion import list_Seccion,add_Seccion,update_Seccion,deleteSeccion
from .views.v_Grado import list_Grado,add_Grado,update_Grado,deleteGrado
from .views.v_directives import list_directives,add_directives,update_directives,deletedirectives
#.

urlpatterns = [
    url(r'^adm_institucion/institucion/listar/$', View_Institucion.as_view()),


  # Classroom
  url(r'^adm_institucion/Classroom/listar/$', list_Classroom.as_view()),
  url(r'^adm_institucion/Classroom/add/$', add_Classroom.as_view()),
  url(r'^adm_institucion/Classroom/update/(?P<pk>\d+)/$', update_Classroom.as_view()),
  url(r'^adm_institucion/Classroom/delete/(?P<id>\d+)/$', deleteClassroom),
  url(r'^adm_institucion/Classroom/alumnos/$', alumnosClassroom.as_view()),
  url(r'^adm_institucion/Classroom/cargaralumnos/$', load_users.as_view()),


  # Seccion
  url(r'^adm_institucion/Seccion/listar/$', list_Seccion.as_view()),
  url(r'^adm_institucion/Seccion/add/$', add_Seccion.as_view()),
  url(r'^adm_institucion/Seccion/update/(?P<pk>\d+)/$', update_Seccion.as_view()),
  url(r'^adm_institucion/Seccion/delete/(?P<id>\d+)/$', deleteSeccion),


  # Grado
  url(r'^adm_institucion/Grado/listar/$', list_Grado.as_view()),
  url(r'^adm_institucion/Grado/add/$', add_Grado.as_view()),
  url(r'^adm_institucion/Grado/update/(?P<pk>\d+)/$', update_Grado.as_view()),
  url(r'^adm_institucion/Grado/delete/(?P<id>\d+)/$', deleteGrado),


  # directives
  url(r'^adm_institucion/directives/listar/$', list_directives.as_view(), name='list_directives'),
  url(r'^adm_institucion/directives/add/$', add_directives.as_view(), name='add_directives'),
  url(r'^adm_institucion/directives/update/(?P<pk>\d+)/$', update_directives.as_view(), name='update_directives'),
  url(r'^adm_institucion/directives/delete/(?P<id>\d+)/$', deletedirectives, name='delete_directives'),
]