from django.db import models

from apps.security.core.core import TimeStampedModel


# Create your models here.
#----Colegios

class Institucion(TimeStampedModel):
    nombre              =models.CharField(max_length=250)
    ruc                 =models.CharField(max_length=11, null=True, blank=True)
    codigo_modular      =models.CharField(max_length=12,blank=True, null=True)
    telefono            =models.CharField(max_length=15, null=True, blank=True)
    direccion           =models.CharField(max_length=100, null=True, blank=True)
    fecha_aniversario   =models.DateField(null=True, blank=True)
    email               =models.CharField(max_length=250, null=True, blank=True)
    ubigeo_id           =models.IntegerField(null=True, blank=True)
    iniciales           =models.CharField(max_length=12, null=True, blank=True)
    niveleducativo      =models.ManyToManyField('NivelEducativo', blank=True)
    logo                =models.ImageField(upload_to='instituciones/logos', blank=True, null=True)
    def __str__(self):
        return self.nombre

    def __validar_dominio__(sb):
        return Institucion.objects.filter(iniciales=sb)

class NivelEducativo(TimeStampedModel):
    nombre =models.CharField(max_length=250)
    def __str__(self):
        return self.nombre

class Grado(TimeStampedModel):
    grado= models.CharField(max_length=250)
    def __str__(self):
        return self.grado

class Seccion(TimeStampedModel):
    seccion=models.CharField(max_length=250)
    def __str__(self):
        return self.seccion

class Classroom(TimeStampedModel):
    name        = models.CharField(max_length=30)
    institucion = models.ForeignKey(Institucion, blank=True, null=True, on_delete=models.CASCADE)
    student     = models.ManyToManyField('adm_security.Users', related_name='students_classroom', blank=True)
    grado       = models.ForeignKey(Grado, on_delete=models.CASCADE)
    seccion     = models.ForeignKey(Seccion, on_delete=models.CASCADE)

    def __str__(self):
        return self.name


    def __get__data__ie__(self, kwargs):
        context = {
            'status':True,
            'institucion_id': self.request.session["institucion"]["id"]
        }
        context.update(kwargs)
        return Classroom.objects.filter(**context).order_by('name')

class directives(TimeStampedModel):
    ie          = models.ForeignKey(Institucion, on_delete=models.CASCADE)
    directive   = models.ForeignKey('adm_security.Users', related_name='directives_ie', blank=True, on_delete=models.CASCADE)
    occupation  = models.CharField(max_length=100)

    def __str__(self):
        return str(self.ie)

    def __get__data__directive__(self, kwargs):
        context = {
            'status':True,
            'ie_id': self.request.session["institucion"]["id"]
        }
        context.update(kwargs)
        return directives.objects.filter(**context)