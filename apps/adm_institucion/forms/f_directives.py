from django import forms
from ..models import directives

class directivesForm(forms.ModelForm):
  class Meta():
    model = directives
    fields = '__all__'
    exclude = ('status', 'deleted_at')