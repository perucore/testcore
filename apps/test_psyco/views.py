# coding=utf-8
import json

from django.shortcuts import HttpResponse,render
from apps.test_psyco.models import Questions,DetalleAltPreg
from apps.adm_test.models import History_test_Patient
from apps.security.core.crud import ListBase, ResponseLoginView
from .models import (Questions, Answers, TestPsicologico, Results, DetalleAltPreg, Valoracion_sistema)
from apps.security.core.processing_test import preProcesamiento
import random
from django.shortcuts import render


#mis test
class MisTest(ListBase):
    template_name = 'Test/MisTest.html'

    def l_queryset(self):
        return History_test_Patient.__test__programed__byusers__(self)

class ShowTest(MisTest):
    template_name = 'Test/ShowTest.html'

class QuestionsTest(ResponseLoginView):
    template_name = 'Test/ShowTest.html'

    def l_queryset(self):
        return Questions.__get__questions__list__(self)

    def response(self,response):
        return self.response_json()

class ApiTest(ResponseLoginView):
    def l_queryset(self):
        return Questions.__get__api__test__(self)

    def response(self,response):
        return self.response_json()

def json_answer(r):
    consulta = TestPsicologico.prepararTest(r,r.GET.get('test'))
    return HttpResponse(json.dumps(list(consulta)))

class save_test(ResponseLoginView):
    def response(self,response):
        try:
            if int(self.request.POST.get('tiempo_ejecucion'))>int(self.request.POST.get('time_marked')):
                print("milton")
                return HttpResponse("Falta completar el tiempo de espera par marcar")
        except ValueError as e:
            pass
        Answers.saveAnswer(self)
        if self.request.POST.get('termino') == 'ya termine':
            Idtest=History_test_Patient.objects.values('test_programed__test').\
                filter(id=self.request.POST.get('history'))[0]['test_programed__test']
            preProcesamiento(self.request, Idtest)
            #Results.enviarAProcesamiento(Results(), self.request, Idtest)
        return HttpResponse("Bien")

def datos_aleatoriosAF5ki(r):
    #determinar el id historial, para eso se ingresara el id de la programacion
    #este dato es estatico XD, se tiene que modificar
    idprogramacion=35;
    #consultamos a todos los estudiantes que estan en esa progamacion
    historial=History_test_Patient.objects.values('id').filter(test_programed=idprogramacion)
    return HttpResponse(json.dumps(list(historial)))


def llenar_base(r):
    k=7
    l=318
    for j in range(163,223):
        for n in range(1,3):
            l=l+1
            k=k+1
            holis=DetalleAltPreg()
            holis.id = l
            holis.status = True
            holis.alternativas_id = k
            holis.preguntas_id = j
            holis.save()
    return HttpResponse("listo")

def emocional_answer(r):
    consulta = TestPsicologico.prepararTest('prueba')

    return HttpResponse(json.dumps(list(consulta)))

def guardar_af5(request):
    #Debes validar la primera entrada
    if Answers.validarRespuestas(Answers, request) == True:
        Answers.saveAnswer(Answers, request)
        if request.POST.get('bandera') == 'yes':

            Results.enviarAProcesamiento(Results(), request)
        return HttpResponse("Bien")
    else:
        return HttpResponse("Mal")

def procesarAf5(request):
    #print("llego")
    #print(request.POST)
    return HttpResponse ("holi")

class result(ListBase):
    template_name = "result.html"

    def l_queryset(self):
        h = History_test_Patient.objects.get(pk=self.request.GET.get('hist'))
        h.status_test = "Realizado"
        h.save()
        return Results.objects.filter(history=self.request.GET.get('hist'), status=True).order_by('-score')

def valor_sistema(request):
    return render(request,'valoracion_sistema.html',{'historial':request.GET.get('historial')})

def guardar_valor(request):
    re = Valoracion_sistema()
    re.idusur = request.user.id
    re.idtest    = 1
    re.idhistorial   = request.POST.get('historial')
    re.question1 = request.POST.get('p1')
    re.question2  = request.POST.get('p2')
    re.question3 = request.POST.get('p3')
    re.question4  = request.POST.get('p4')
    re.question5 = request.POST.get('p5')
    re.save()
    return render(request,"mensajevaloracion.html")

def migrar_valora(request):
    datos=History_test_Patient.objects.filter(test_programed=32,patient__profiles=6).exclude(answers__value_marked__isnull=True).values("id","patient")
    cont=0
    with open('E:\datoscachay.csv','r') as f:
        file=f.read().splitlines()
        for l in file:
            file=l.split(";")
            re = Valoracion_sistema()
            re.idusur =datos[cont]["patient"]
            re.idtest    = 1
            re.idhistorial   =datos[cont]["id"]
            re.question1 = file[0]
            re.question2  = file[1]
            re.question3 = file[2]
            re.question4  = file[3]
            re.question5 = file[4]
            re.save()
            cont=cont+1
    pass

def relacionar(r):

    datos=Questions.objects.filter(idtest=13).values("id").order_by("correlativo")
    for foo in datos:
        print(foo['id'])
        da=DetalleAltPreg()
        da.alternativas_id=590
        da.preguntas_id=foo["id"]
        da.puntaje=1
        da.save()

        de=DetalleAltPreg()
        de.alternativas_id=596
        de.preguntas_id=foo["id"]
        de.puntaje=2
        de.save()

        di=DetalleAltPreg()
        di.alternativas_id=597
        di.preguntas_id=foo["id"]
        di.puntaje=3
        di.save()



    pass