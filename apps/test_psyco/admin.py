from django.contrib import admin
from .models import Questions,RangeAge, ImagesQuestion, Dimensions, TestPsicologico,\
    Alternativas, Diagnostics, TipoAlternativas, DetalleVarTest, RangosPercentiles

class SubDimensionesFilter(admin.SimpleListFilter):
    title = 'dimensiones'
    parameter_name = 'dimensiones__id__exact'

    def lookups(self, request, model_admin):
        dimensiones = Dimensions.objects
        if 'idtest__id__exact' in request.GET:
            dimension = dimensiones.filter(test=request.GET['idtest__id__exact'])
        else:
            dimension = dimensiones.all()
        return (
            (subd.pk,subd.nombre) for subd in dimension
        )

    def queryset(self, request, queryset):
        if 'dimensiones__id__exact' in request.GET:
            return queryset.filter(dimensiones=request.GET['dimensiones__id__exact'])


class PreguntaInlineAdmin(admin.TabularInline):
    model = Questions.imagen.through

class AlternativaInlineAdmin(admin.TabularInline):
    model = Questions.alternativa.through


class PreguntaAdmin(admin.ModelAdmin):
    inlines = (PreguntaInlineAdmin,AlternativaInlineAdmin,)
    list_display = ('id', 'descripcion', 'correlativo')
    list_filter = (
        'idtest',SubDimensionesFilter
    )

class testInlineAdmin(admin.TabularInline):
    model = TestPsicologico.variable.through


class TestAdmin(admin.ModelAdmin):
    inlines = (testInlineAdmin,)


class RangoAdmin(admin.ModelAdmin):
    list_filter = (
        'det_variables','dimensiones'
    )

class DimensionesAdmin(admin.ModelAdmin):
    list_filter = (
        'test',
    )


class DiagnosticoAdmin(admin.ModelAdmin):
    list_filter = (
        'dimensiones__test',
        'dimensiones'
    )

# Register your models here.
admin.site.register(Questions, PreguntaAdmin)
admin.site.register(TestPsicologico, TestAdmin)
admin.site.register(RangeAge)
admin.site.register(ImagesQuestion)
admin.site.register(Dimensions, DimensionesAdmin)
admin.site.register(Alternativas)
admin.site.register(Diagnostics, DiagnosticoAdmin)
admin.site.register(TipoAlternativas)
admin.site.register(DetalleVarTest)
admin.site.register(RangosPercentiles,RangoAdmin)
