from django.conf.urls import url
from .views import (save_test, MisTest, ShowTest, QuestionsTest,valor_sistema,guardar_valor,migrar_valora,
                    json_answer, result, procesarAf5, emocional_answer, llenar_base,datos_aleatoriosAF5ki,
                    relacionar, ApiTest)
from apps.security.core.template import Vista

urlpatterns = [
    url(r'^test_psyco/af5/mostraraf5$',Vista.as_view(template_name="mostrar_af5.html")),
    url(r'^test_psyco/emocional/mostrar_emocionas$', Vista.as_view(template_name="mostrar_emocional.html")),
    url(r'^test_psyco/af5/mostrar_emocionalprocesar$', emocional_answer),

    url(r'^test_psyco/test/json_answer', json_answer),#preguntas en json


    url(r'^test_psyco/af5/procesarAf5/$',procesarAf5),
    url(r'^test_psyco/catell2/mostrarcatell2$', Vista.as_view(template_name="mostrar_catellII.html")),
    url(r'^test_psyco/CPM/mostrarCPM$', Vista.as_view(template_name="mostrar_CPM.html")),



    url(r'^test_psyco/raven/guardar_test/$', save_test),
    url(r'^test_psyco/raven/mostrarraven$', Vista.as_view(template_name="mostrar_raven.html")),
    url(r'^test_psyco/af5/procesarAf55/$',llenar_base),

    url(r'^test_psyco/mistest/listar/$',MisTest.as_view()),
    url(r'^test_psyco/test/show/$',ShowTest.as_view()),

    url(r'^test_psyco/test/questions/$', QuestionsTest.as_view()),

    url(r'^test_psyco/test/guardar_test/$', save_test.as_view()),  # guardar test
    url(r'^test_psyco/test/result/', result.as_view()),

    #prueba
    url(r'^test_psyco/test/datos_aleatoriosAF5/', datos_aleatoriosAF5ki),

    url(r'^test_psyco/test/varoralcion_sistema/', valor_sistema),
    url(r'^test_psyco/test/guardar_valoracion/', guardar_valor),
    url(r'^test_psyco/test/migrar_valoracion/', migrar_valora),
    url(r'^test_psyco/test/reacionar/', relacionar),

    url(r'^test/api/$', ApiTest.as_view()),
]