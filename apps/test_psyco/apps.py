from django.apps import AppConfig


class TestPsycoConfig(AppConfig):
    name = 'test_psyco'
