from django.contrib.postgres.fields import *
from django.db import models

from apps.security.core.core import TimeStampedModel
from  apps.security.core.core import edadActual, current_age



class TestPsicologico(TimeStampedModel):
    name             = models.CharField(max_length=50)
    description      = models.CharField(max_length=300)
    instructions     = models.TextField()
    es_cronometrado  = models.BooleanField(default=False)
    variable         = models.ManyToManyField('Variables', through='DetalleVarTest')# agregue para meter admin django
    level_correction = models.CharField(max_length=100,null=True, blank=True)
    formul_unique    = models.CharField(max_length=250, null=True, blank=True)
    limit_time       = models.IntegerField(null=True, blank=True)

    def __str__(self):
        return self.name

    def __list__test__(self):
        return [(i.id, i.name) for i in TestPsicologico.objects.filter(status=True)]

class RangeAge(models.Model):
    range = IntegerRangeField()

    def __str__(self):
        return str(self.range)


class ImagesQuestion(models.Model):
    description = models.CharField(max_length=60)
    image       = models.ImageField(upload_to="test")
    ranges      = models.ForeignKey('RangeAge', blank=True, null=True, on_delete=models.CASCADE)

    def __str__(self):
        return self.description


class TipoAlternativas(TimeStampedModel):
    nombre = models.CharField(max_length=20)

    def __str__(self):
        return self.nombre

class Alternativas(TimeStampedModel):
    descripcion = models.CharField(max_length=300)
    acronimo = models.CharField(max_length=2)
    escala = IntegerRangeField(blank=True, null=True)
    tipo = models.ForeignKey('TipoAlternativas', on_delete=models.CASCADE)
    imagen = models.ImageField(upload_to="ImagenesAlternativa", null=True, blank=True)
    puntajeAsignado=models.DecimalField(null=True,max_digits=10, blank=True,decimal_places=2)
    #este es para poner al test con preguntas con multiples dimensiones
    #pero no soporta formula solo sumatoria
    iddimension=models.ForeignKey('Dimensions', null=True, blank=True, on_delete=models.CASCADE)

    def __str__(self):
        return self.descripcion


class DetalleImagPreg(models.Model):
    preguntas = models.ForeignKey('Questions', on_delete=models.CASCADE)
    imagenes = models.ForeignKey('ImagesQuestion', on_delete=models.CASCADE)

class DetalleAltPreg(TimeStampedModel):
    preguntas = models.ForeignKey('Questions', on_delete=models.CASCADE)
    alternativas = models.ForeignKey('Alternativas', on_delete=models.CASCADE)
    puntaje      = models.IntegerField(blank=True, null=True)

    def listarAlternativas(self,preguntas):
        return self.objects.filter(preguntas=preguntas)



class Questions(TimeStampedModel):
    pregunta = models.ForeignKey('self',null=True, blank=True, on_delete=models.CASCADE)
    tiene_hijos = models.BooleanField(default=False)
    descripcion = models.CharField(max_length=300)
    correlativo = models.IntegerField()
    dimensiones = models.ForeignKey('Dimensions', null=True, blank=True, on_delete=models.CASCADE)
    tiempo_ejecucion = models.IntegerField(null=True, blank=True)
    imagen = models.ManyToManyField('ImagesQuestion', through='DetalleImagPreg')
    alternativa = models.ManyToManyField('Alternativas', through='DetalleAltPreg')
    idtest=models.ForeignKey(TestPsicologico,null=True,blank=True, on_delete=models.CASCADE)

    def __str__(self):
        return self.descripcion

    def __get__questions__list__(self):
        pre = Questions.objects.filter(status=True, idtest_id=self.request.GET.get('test')).order_by('?')#.order_by('correlativo')#query for get values of Questions
        dimg = DetalleImagPreg.objects #query for get images from of detail
        dalt = DetalleAltPreg.objects #query for get images from of detail

        li = [] #new list for create json
        for nl in pre.values():
            da = dalt.filter(preguntas_id=nl['id']).values('id','puntaje',
            'alternativas__descripcion',
            'alternativas__acronimo','alternativas__escala','alternativas__tipo__nombre',
            'alternativas__puntajeAsignado','alternativas__imagen',
            'alternativas__iddimension_id', 'alternativas__iddimension__nombre'
                                                           )
            nl['alternativas'] = list(da)
            ii = dimg.filter(preguntas_id=nl["id"]).values('imagenes__description','imagenes__image',
                                                        'imagenes__ranges__range'
            )#filter images by id questions
            nli= {}#new dict for save images by age
            ra = None
            if ii:
                nl["imagenes_pregunta"] = ''
                for r in ii:
                    if  r['imagenes__ranges__range'] is not None:
                        rango = str(r['imagenes__ranges__range'].lower) + "-" + str(r['imagenes__ranges__range'].upper)
                        aa = r['imagenes__ranges__range'].lower <= current_age(
                        self.request.user.birth_date) <= r['imagenes__ranges__range'].upper
                        if aa:
                           if rango in nli:
                              nli[rango].append(r['imagenes__image'])
                           else:
                              nli[rango] = [r['imagenes__image']]
                           ra = rango
                    else:
                        nl["imagenes_pregunta"] = [r['imagenes__image']]

                if ra is not None: nl["imagenes_pregunta"]= nli[ra]
            li.append(nl)# saved all questions with your images
        return li

    def __get__api__test__(self):
        pre = Questions.objects.filter(status=True, idtest_id=self.request.GET.get('test')).order_by('?')#.order_by('correlativo')#query for get values of Questions
        dimg = DetalleImagPreg.objects #query for get images from of detail
        dalt = DetalleAltPreg.objects #query for get images from of detail
        test = TestPsicologico.objects.filter(id=self.request.GET.get('test')) #query for get Test
        dime = Dimensions.objects.filter(test_id=self.request.GET.get('test')) # query for get all Dimensions
        diag = Diagnostics.objects # query for get all Diagnostic
        perc = RangosPercentiles.objects # query for get all ranges

        vari = DetalleVarTest.objects.filter(tests_id=self.request.GET.get('test')) # query for get all variables of test
        vara = [v for v in vari.values('variables_id__variable','variables_id__descripcion','variables_id__categoria_id__nombre')]

        test = list(test.values('name', 'description', 'instructions', 'level_correction'))[0]
        test['variables'] = vara

        ndi = []
        for dd in dime.values('id','nombre', 'descripcion', 'formula'):
            ndia = diag.filter(dimensiones_id=dd['id']).values('id','nombre','descripcion','color')

            lnd = []
            for s in ndia:
                s['centiles'] = [p for p in perc.filter(diagnostics_id = s['id']).values('puntuacion')]
                del s["id"]
                lnd.append(s)

            del dd["id"]
            dd['diagnosticos'] = lnd
            ndi.append(dd)

        test['dimensiones'] = ndi

        apitest = {'test':test}
        li = [] #new list for create json
        for nl in pre.values('id','descripcion','correlativo','dimensiones_id__nombre','tiempo_ejecucion'):
            da = dalt.filter(preguntas_id=nl['id']).values('puntaje',
            'alternativas__descripcion',
            'alternativas__acronimo','alternativas__escala','alternativas__tipo__nombre',
            'alternativas__puntajeAsignado','alternativas__imagen',
            'alternativas__iddimension__nombre'
                                                           )
            nl['alternativas'] = list(da)
            ii = dimg.filter(preguntas_id=nl["id"]).values('imagenes__description','imagenes__image',
                                                        'imagenes__ranges__range'
            )#filter images by id questions
            nli= {}#new dict for save images by age
            ra = None
            if ii:
                nl["imagenes_pregunta"] = ''
                for r in ii:
                    if  r['imagenes__ranges__range'] is not None:
                        rango = str(r['imagenes__ranges__range'].lower) + "-" + str(r['imagenes__ranges__range'].upper)
                        aa = r['imagenes__ranges__range'].lower <= current_age(
                        self.request.user.birth_date) <= r['imagenes__ranges__range'].upper
                        if aa:
                           if rango in nli:
                              nli[rango].append(r['imagenes__image'])
                           else:
                              nli[rango] = [r['imagenes__image']]
                           ra = rango
                    else:
                        nl["imagenes_pregunta"] = [r['imagenes__image']]

                if ra is not None: nl["imagenes_pregunta"]= nli[ra]

            del nl["id"]

            li.append(nl)# saved all questions with your images
        apitest['preguntas'] = li
        return apitest

class Answers(TimeStampedModel):
    date_answer    = models.DateField(auto_now=True)
    time_marked    = models.IntegerField()
    answer         = models.ForeignKey("DetalleAltPreg", null=True, blank=True, on_delete=models.CASCADE)
    history        = models.ForeignKey('adm_test.History_test_Patient', blank=True, null=True, on_delete=models.CASCADE)
    value_marked   = models.CharField(max_length=10,blank=True, null=True)


    def validarRespuestas(self,dixy):
        valido = True
        for item,value_marked in dixy.POST.items():
            if value_marked == '' or value_marked == 'undefined':
                valido = False
                break
        return valido

    def saveAnswer(self):
        dixy = self.request.POST
        re = Answers()
        re.value_marked = dixy.get('value_marked')
        re.answer_id    = dixy.get('answer')
        re.history_id   = dixy.get('history')
        re.value_marked = dixy.get('value_marked')
        re.time_marked  = dixy.get('time_marked')
        re.save()
        pass


class Results(TimeStampedModel):
    diagnostics        = models.ForeignKey('Diagnostics', null=True, on_delete=models.CASCADE)
    time              = models.IntegerField(null=True)
    history           = models.ForeignKey('adm_test.History_test_Patient', null=True, blank=True, on_delete=models.CASCADE)
    score             = models.FloatField(null=True)


    def enviarAProcesamiento(self,dixy,idtest):
        #fecha_hoy = timezone.now().date()#-timezone.timedelta(days=1)
        #consulta = Answers.objects.values().filter(test=dixy.POST.get('idtest'), \
         #                                          idprogramacion=dixy.POST.get('idprogramacion'), \
          #                                         paciente=dixy.user.id).order_by("correlativo")
        consulta=Answers.objects.values("value_marked","answer__preguntas__dimensiones",\
                                        "answer__alternativas__iddimension").\
            filter(history=dixy.POST.get('history')).order_by("answer__preguntas__correlativo")
        #dimensiones = Dimensiones.objects.filter(test=dixy.POST.get('idtest'))
        #por el momento trabajare con un id test estatico
        dimensiones=Dimensions.objects.filter(test=idtest)
        #esto es por el momento mientras cachay este pensando
        cantidadPregunta=dimensiones.values("questions__correlativo").count()

        #dim = Dimensiones.objects.values().filter(test=dixy.POST.get('idtest'))
        dim = Dimensions.objects.values().filter(test=idtest)
        self.procesarGeneral(consulta,cantidadPregunta,dim,dixy,idtest)

    #funcion para procesar cualquier test con parametros predefinidos XD
    def procesarGeneral(self,consulta,cPregunta,dimension,request,idtest):
        resultados=[]
        pi={}
        puntajedimension={}
        #verificamos si la pregunta tiene codigo de dimension
        #si no tiene es por que es un test que tienen las dimensiones en la alternativa
        if consulta.values("answer__preguntas__dimensiones")[1]["answer__preguntas__dimensiones"]!=None:
            for numeroPregunta in range(cPregunta):

                if not consulta[numeroPregunta:numeroPregunta+1]:
                    pi[numeroPregunta+1]=0
                else:
                    pi[numeroPregunta+1]=int(consulta[numeroPregunta]["value_marked"])
            for dim in dimension:
                puntajedimension[dim["id"]]=eval(str(dim["formula"]))
        else:
            for dim in dimension:
                #falta sumar pero como no supe como hacer consulta con SUM ice con count
                #que no es lo mismo pero por el momento
                puntajedimension[dim["id"]]=consulta.values().filter(answer__alternativas__iddimension=dim["id"]).\
                    count()
        testpsicologico=TestPsicologico.objects.values().\
                filter(id=idtest)
        #valida si hay 0 respuestas para dar un mensaje(comprobar)
        if consulta.count()== 0:
            guardar=Observaciones_test()
            guardar.observacion="No ingreso ningun dato"
            guardar.responsable=request.user.id
            guardar.det_programa_test=request.POST.get("iddetpro")
            #guardar.historial_id=str(Historial.objects.get(paciente_id=request.user.id))
            guardar.save()
            return False
        if  testpsicologico[0]["level_correction"]== "Unica":
            resultados.append({'dimid': None,
                                   'puntaje': eval(str(testpsicologico[0]["formulaunica"]))})
            #falta verificar si es para puntuacion final o para pregunta
            #la validacion de puntaje final solo funcionara para test que tienen resultado unica
            if testpsicologico.values('validacion_pf__id').\
                    filter(id=idtest)[0]['validacion_pf__id'] :
                for validar in testpsicologico.values('validacion_pf__puntuacion','validacion_pf__validacion',
                                                      'validacion_pf__mensaje').filter(id=idtest):
                    validador=validar['validacion_pf__puntuacion']
                    if float(validador.split('-')[0])<=resultados[0]['puntaje']\
                            and float(validador.split('-')[1])>resultados[0]['puntaje']:
                        if eval(str(validar['validacion_pf__validacion'])):
                            pass
                        else:
                            guardar=Observaciones_test()
                            guardar.observacion=validar['validacion_pf__mensaje']
                            guardar.responsable=request.user.id
                            guardar.det_programa_test=request.POST.get("iddetpro")
                            #guardar.historial_id=str(Historial.objects.get(paciente_id=request.user.id))
                            guardar.save()
        else:
            for x,y in puntajedimension.items():

                resultados.append({'dimid': x,
                                   'puntaje': y})
        #Falta comprobar, es que tengo pereza llenar la base de datos
        if testpsicologico.values('validacion_pf__id').\
                    filter(id=idtest)[0]['validacion_pf__id'] :
            for validar in testpsicologico.values('validacion_pf__validacion',
                                                      'validacion_pf__mensaje').filter(id=idtest):
                if eval(str(validar['validacion_pf__validacion'])):
                    pass
                else:
                    guardar=Observaciones_test()
                    guardar.observacion=validar['validacion_pf__mensaje']
                    guardar.responsable=request.user.id
                    guardar.det_programa_test=request.POST.get("iddetpro")
                    #guardar.historial_id=str(Historial.objects.get(paciente_id=request.user.id))
                    guardar.save()
        self.generarDiagnostico(resultados,request,idtest)


    def verificacionpuntuacion(self):
        pass

    def calcularPuntajeAF5(self,consulta, dimensiones, request):
        resultados = []
        for dimension in dimensiones:
            puntaje = 0
            for resp in consulta:
                if resp.dimension == dimension.id:
                    if resp.correlativo not in {4, 12, 14, 22}:
                        puntaje = puntaje + int(resp.valor_marcado)
                    else:
                        puntaje = puntaje + (100- int(resp.valor_marcado))
            if dimension.nombre == 'Emocional':
                resultados.append({'dimid':dimension.id,
                                   'dimnombre':dimension.nombre,
                                   'puntaje':round((600-puntaje)/60, 2)})
            else:
                resultados.append({'dimid': dimension.id,
                                   'dimnombre': dimension.nombre,
                                   'puntaje': round(puntaje/60, 2)})
        self.generarDiagnostico(resultados,request)
        return

    #funcion de milton para generar el diagnostico utilizando funcion echas por goofito XD
    def generarDiagnostico(self,resultados,request,idtest):
        #identificamos que tipo de nivel de revicion tiene para determinar que funciones utilizar
        #por el momento solo usaremos el nivel de correccion "dimension puntaje mayor
        if TestPsicologico.objects.values('level_correction').\
                filter(id=idtest)[0]['level_correction']!="Dimension puntaje mayor":

            #trae todas las variables con sus respectiva paternidad, del respectivo test
            variablestest=DetalleVarTest.objects.filter(tests=idtest)
            #trae las puntuaciones, diemensiones, diagnosticos de la respectiva variable a la que el usuario pertenece
            datosdiagnostico= self.diagnosticoDimencionVariable(variablestest,request)
            #trae un array con los diagnosticos del paciente
            rangopuntuacion=self.rangopuntuacion(resultados,datosdiagnostico)
        else:
            rangopuntuacion=self.rangopuntuacionDimensionMayor(resultados)
        #guarda en la tabla resultados y trae un mensaje
        mensajeguardado=self.guardarDatosResultado(rangopuntuacion,request)
        return mensajeguardado

    def diagnosticoDimencionVariable(self,variable,request):
        #identificamos al padre
        for var in variable.values('variables__descripcion',
                                   'id','variables__categoria__nombre',
                                   'variables__variable',
                                   'variables__categoria__descripcion').\
                filter(padre=None):
            #identifica si coinsiden
            dato=self.identificar(var,request)
            if dato == "iguales":
                #
                puntuaciondimension=self.tienehijos(var,variable,request)
                break
        return puntuaciondimension

    def rangopuntuacionDimensionMayor(self,resultado):
        guardarResultado=[]
        for res in resultado:
                diagnostico=Dimensions.objects.values("diagnostics__id").filter(id=res['dimid'])
                res['diagnostics_id']=diagnostico[0]["diagnostics__id"]
                res['dimensions_id']=res['dimid']
                guardarResultado.append(res)
            #falta cuando es igual. Cuando es igual tiene que guardar los dos cuando va por primera ves
        return guardarResultado

    #Falta conectar la session con la base de datos, identificar la edad falta
    def identificar(self,variable,request):
        #Buscar el nombre de la session para poder comparar con las variables
        #falta identificar si es edad para calcularlo
        #Por el momento pondre el nombre correspondiente para avanzar XD

        #verifica la session correspondiente es igual a la variable y retorna un mensaje de
        if variable['variables__variable']=="rango":
            numeros= variable['variables__descripcion'].split('-')
            if variable['variables__categoria__descripcion']== "edad":
                edad=edadActual(request.user.date_birth)
                if float(numeros[0])< edad and float(numeros[1])>edad:
                    data="iguales"
                else:
                    data="diferente"
        elif variable['variables__variable']=="generica":
            #no se que estoy haciendo : (
            data="iguales"
        else:
            var = "request.user."+str(variable['variables__categoria__nombre'])
            if eval(var) == variable['variables__descripcion']:
                data="iguales"
            else:
                data="diferente"
        return data

    #falta verificar si pasa la parte de tiene hijos
    def tienehijos(self,var,variable,request):
        #trae todos los datos que no sean padres
        for vari in variable.values('padre','variables__descripcion').exclude(padre=None):
            #identifica si alguno de los registros tiene como padre al "var['id']"
            if vari['padre'] == var['id']:
                #envia a preguntar si son iguales con el dato del paciente y trae un mensaje
                datomensaje=self.identificar(vari,request)
                if datomensaje=="iguales":
                    #si son iguales no seguimos con los hijos del padre, ahora el padre es el hijo que coinsido con
                    #los datos del paciente
                    self.tienehijos(vari,variable,request)
        #cuando ya no haya hijos se trae datos de "rangopercentil" con la variable padre que no tenga hijos
        dimensiones=RangosPercentiles.objects.\
            values('puntuacion','dimensiones__nombre','det_variables_id','dimensiones_id').\
            filter(det_variables=var['id'])
        return dimensiones

    # sacaremos sus respectivo diagnosticos y lo retornaremos en un array
    def rangopuntuacion(self,resultado,diagnostico):
        guardarResultado=[]
        #"resultado" tiene las dimensiones y su respectiva puntuaciones
        for resl in resultado:
            #trae los datos del diagnostico al que pertenece su puntuacion con respecto a la variable y dimension
            resultadopuntuacion=self.verificarPuntuacionRago(resl,diagnostico)
            #Este array ira incrementando con las respectivos diagnosticos de todas las dimensiones
            guardarResultado.append(resultadopuntuacion)
        return guardarResultado

    #Identifica en que rango esta la puntuacion para retornar el iddimension, iddiagnostico,idvariable
    def verificarPuntuacionRago(self,resultado,diagnostico):
        #"iddiganostico" trae todos los id de los diagnosticos que pertenece a una dimension
        iddiganostico=diagnostico.values("diagnostics_id").filter(dimensiones_id=resultado['dimid']).\
            order_by('puntuacion')
        iddiafunico=[]
        #"iddiganostico" nos trae ids con respecto al "rango percentil" por lo cual tiende a repetirse
        #los cual esto une todos los ides repetidos y crea un array con los ids unicos
        print(iddiganostico)
        for iddig in iddiganostico:
            if not iddig['diagnostics_id'] in iddiafunico:
                iddiafunico.append(iddig['diagnostics_id'])
        primernumero=0
        res={}
        for rango in iddiafunico:
            #"rangoXDiacnostico" trae las puntuaciones de un diagnostico que va de menor a mayor(esto fue echo arriba)
            rangoXDiacnostico=diagnostico.values('dimensiones_id','diagnostics_id','puntuacion').\
                filter(diagnostics_id=int(rango),dimensiones_id=resultado['dimid']).order_by('-puntuacion')
            #cogemos el primer registro ya que seria el limite de puntuacion del primer diagnostico
            #se hace un rango para verificar si es que el puntaje pertenece al rango correspondiente
            if resultado['puntaje']>primernumero and resultado['puntaje']<=rangoXDiacnostico[0]['puntuacion']:
                #si pertenece retorna la consulta echa al diagnostico
                res['dimensiones_id']=rangoXDiacnostico[0]['dimensiones_id']
                res['diagnostics_id']=rangoXDiacnostico[0]['diagnostics_id']
                res['puntaje']=resultado['puntaje']
                return res
            primernumero=rangoXDiacnostico[0]['puntuacion']
            pass

    def guardarDatosResultado(self,datos,request):
        for d in datos:
            guar=Results()
            guar.score = d['puntaje']
            guar.diagnostics_id =d['diagnostics_id']
            guar.time  =12#tambien falta determinar
            guar.history_id =int(request.POST.get('history'))
            guar.save()


        #crear una funcion en la views de programacion y traerlo
        mensaje="Guardado correctamente"
        return mensaje

    def procesarVocacional(self,dixy):
        dimr = dimi = dima = dims = dime = dimc = 0

        # preguntasxdimension = consulta que nos otorga todas las
        # preguntas que pertenecen a la dimension consultada, en
        # un resultado parecido a las matrices de abajo.

        # Podriamos poner esto en una tabla en base de datos?
        a1 = [1,2,3,4,5,6,7,8,9,10]
        a2 = [11,12,13,14,15,16,17,18,19,20]
        a3 = [21,22,23,24,25,26,27,28,29,30]
        a4 = [31,32,33,34,35,36,37,38,39,40]
        a5 = [41,42,43,44,45,46,47,48,49,50]
        a6 = [51,52,53,54,55,56,57,58,59,60]

        # Todas las respuestas a las preguntas tienen peso uno
        # por lo tanto se suman, de acuerdo al numero de pregunta
        for fila in dixy:
            if (fila['numero'] in a1) and (fila['valor'] == 1):
                dimr = dimr + 1
            elif (fila['numero'] in a2) and (fila['valor'] == 1):
                dimi = dimi + 1
            elif (fila['numero'] in a3) and (fila['valor'] == 1):
                dima = dima + 1
            elif (fila['numero'] in a4) and (fila['valor'] == 1):
                dims = dims + 1
            elif (fila['numero'] in a5) and (fila['valor'] == 1):
                dime = dime + 1
            else:
                if fila['valor'] == 1:
                    dimc = dimc + 1

        # El area que tenga el mayor puntaje, el "maximus" será su resultado.
        # Faltaría evaluar si el valor máximo se repite, de ser así podría
        # tener multiples resultados.
        maximus = max([dimr, dimi, dima, dims, dime, dimc])

        # Creamos el diccionario necesario para guardar los datos en la Clase
        # Resultados
        nuevodixy = {'R': dimr,
                     'I': dimi,
                     'A': dima,
                     'S': dims,
                     'E': dime,
                     'C': dimc}
        self.guardarResultados(nuevodixy)

        return

    def procesarRosemberg(self,dixy):
        pass

    def procesarRaven(self,request,respuestas):
        resultados=[]
        respuesta={23:{1:4,2:5,3:1,4:2,5:6,6:3,7:6,8:2,9:1,10:3,11:4,12:5},
                   24:{13:4,14:5,15:1,16:6,17:2,18:1,19:3,20:4,21:6,22:3,23:5,24:2},
                   25:{25:2,26:6,27:1,28:2,29:1,30:3,31:5,32:6,33:4,34:3,35:4,36:5}}
        #captrar tiempo

        #edad paciente
        #print(str(float((datetime.date.today() - dixy.user.date_birth).days/365)))
        #diff = datetime.date.today() - dixy.user.date_birth
        #sacar el puntaje
        contA=0
        contAB=0
        contB=0

        for foo in respuestas:
            if int(foo.dimension) == 23:
                if int(foo.valor_marcado) == respuesta[23][foo.correlativo]:
                    contA=contA+1
            if int(foo.dimension)== 24:
                if int(foo.valor_marcado) == respuesta[24][foo.correlativo]:
                    contAB=contAB+1
            if int(foo.dimension)== 25:
                if int(foo.valor_marcado) == respuesta[25][foo.correlativo]:
                    contB=contB+1
        #diacnostico
        resultados.append({'dimid': None,
                                   'dimnombre': None,
                                   'puntaje': contA+contAB+contB})
        self.generarDiagnostico(resultados,request)

        pass

    def calcularDiagnostico(self,resultados,request):
        variables = DetalleVarTest.objects.filter(tests=request.POST.get('idtest'))

        for variable in variables:
            if variable.variables.abreviatura != None:
                if request.user.sex == variable.variables.abreviatura:
                    for dim in resultados:
                        percentiles = RangosPercentiles.objects.filter(dimensiones=dim['dimid'],
                                                             variables=variable.variables.id)

                        for percentil in percentiles:
                            if percentil.rango.lower <= dim['puntaje'] <= percentil.rango.upper:
                                diagnostico = percentil.diagnostics.descripcion

                elif variable.variables.variable == 'Universal':
                    pass
            elif variable.variables.rango != None:
                if self.edadActual(request.user.date_birth) in \
                        range(variable.variables.rango.lower,
                              variable.variables.rango.upper -1):
                    pass
        return

    def guardarResultado(self):
        pass


class Diagnostics(TimeStampedModel):
    dimensiones = models.ForeignKey('Dimensions', null=True, on_delete=models.CASCADE)
    nombre = models.CharField(max_length=20)
    descripcion = models.TextField()
    color       = models.CharField(max_length=20, null=True, blank=True)
    rango = FloatRangeField(null=True, blank=True)

    def __str__(self):
        return str(self.dimensiones) + ' - ' + str(self.nombre)

class Dimensions(TimeStampedModel):
    nombre = models.CharField(max_length=30)
    descripcion = models.TextField()
    test = models.ForeignKey('TestPsicologico', on_delete=models.CASCADE)
    formula=models.CharField(max_length=1000,null=True, blank=True)

    def __str__(self):
        return self.nombre

    def cuantasDimensionesTengo(self,idtest):
        return self.objects.filter(test=idtest).count()

class DetalleVarTest(TimeStampedModel
                     ):
    tests = models.ForeignKey('TestPsicologico', on_delete=models.CASCADE)
    variables = models.ForeignKey('Variables', on_delete=models.CASCADE)
    padre = models.IntegerField(null=True,blank=True)

    def __str__(self):
        return str(self.tests)

class Variables(TimeStampedModel):
    variable = models.CharField(max_length=30)
    abreviatura = models.CharField(max_length=3,null=True)
    rango = FloatRangeField(null=True)
    descripcion = models.CharField(max_length=200)
    categoria = models.ForeignKey('CategoriaVariables',null=True, on_delete=models.CASCADE)

    def __str__(self):
        return self.variable

class CategoriaVariables(TimeStampedModel):
    nombre = models.CharField(max_length=30)
    descripcion = models.CharField(max_length=300)


class Centiles(TimeStampedModel):
    valor=models.IntegerField()

class RangosPercentiles(TimeStampedModel):
    det_variables = models.ForeignKey('DetalleVarTest', on_delete=models.CASCADE)
    dimensiones = models.ForeignKey('Dimensions', null=True, on_delete=models.CASCADE)
    diagnostics = models.ForeignKey('Diagnostics', null=True, on_delete=models.CASCADE)
    descripcion = models.TextField()
    puntuacion=models.DecimalField(null=True,max_digits=10, decimal_places=2)
    centiles=models.ForeignKey('Centiles',null=True, blank=True, on_delete=models.CASCADE)

    def __str__(self):
        return str(self.det_variables) + ' - ' + str(self.diagnostics) + ' - ' + str(self.puntuacion)

class Type_validation(models.Model):
    description=models.CharField(max_length=200)


#esta tabla es para validar la puntuacion final teniendo como campo puntuacion y validacion
#para la validacion de preguntas solo utilizaremos el campo validacion
class Validacion_pf(TimeStampedModel):
    puntuacion=models.CharField(max_length=20,null=True)
    validacion=models.TextField()
    mensaje=models.CharField(max_length=250)
    testpsicologico=models.ForeignKey(TestPsicologico, on_delete=models.CASCADE)
    proceso=models.CharField(max_length=20)
    type_validation=models.ForeignKey(Type_validation, on_delete=models.CASCADE)
    father=models.IntegerField(null=True)

#esto es observaciones de cada tes para cada persona que hizo el test
class Observaciones_test(TimeStampedModel):
    observacion=models.TextField()
    #maquina null y personal responsable id user
    responsable=models.IntegerField(null=True)
    #historial=models.ForeignKey(Historial)
    det_programa_test=models.IntegerField()


class Valoracion_sistema(TimeStampedModel):
    idusur=models.IntegerField()
    idtest=models.IntegerField()
    idhistorial=models.IntegerField()
    question1=models.IntegerField()
    question2=models.IntegerField()
    question3=models.IntegerField()
    question4=models.IntegerField()
    question5=models.IntegerField()
#class DiccionarioPsicologico(models.Model):
    #pass

#Fin Goofito
