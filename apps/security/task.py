from django.db import connection
from config.celery import app


@app.task
def my_task():
    print(connection.schema_name)