from django import forms
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth import (
    authenticate, password_validation
)
from django.contrib.auth import get_user_model
Users = get_user_model()
from .models import profile, modules, permission
S = (
    ('m', 'Masculino'),
    ('f', 'Femenino'),
)
def listF(a, b):
    d = {'class': 'form-control input-sm'}
    d[a] = b
    return d

class addUser(forms.ModelForm):
    email = forms.EmailField(
        help_text=('Email'),
        required=True,
        widget=forms.EmailInput(
            attrs={'class': 'form-control input-sm', 'placeholder': 'ingresar dirección de correo Electrónico'}),
    )

    sex = forms.ChoiceField(
        choices=S,
        label="Sexo",
        widget=forms.Select(attrs=listF('placeholder', '')))


    class Meta:
        model = Users
        fields = ('names', 'first_surname', 'second_surname',  'dni', 'sex', 'birth_date','profiles')
        #exclude = ('email',)
        password1 = forms.CharField(widget=forms.HiddenInput(), required=False)
        password2 = forms.CharField(widget=forms.HiddenInput(), required=False)
        username = forms.CharField(widget=forms.HiddenInput(), required=False)

        widgets = {
            'names': forms.TextInput(attrs=listF('placeholder', 'ingrese nombres completos')),
            'first_surname': forms.TextInput(attrs=listF('placeholder', 'ingrese apellido paterno')),
            'second_surname': forms.TextInput(attrs=listF('placeholder', 'ingrese apellido materno')),
            'birth_date': forms.DateInput(format=('%Y-%m-%d'), attrs={'type': 'date'}),
            'dni': forms.TextInput(attrs=listF('placeholder', 'ingrese DNI')),
        }









class CustomerCreationForm(forms.ModelForm):
    password1 = forms.CharField(label=("Password"), widget=forms.PasswordInput)
    password2 = forms.CharField(label=("Password confirmation"), widget=forms.PasswordInput,
                                help_text=("Enter the same password as before, for verification."))

    email = forms.EmailField(
        help_text=('Email'),
        required=True,
        widget=forms.EmailInput(
            attrs={'class': 'form-control input-sm', 'placeholder': 'ingresar dirección de correo Electrónico'}),
    )

    sex = forms.ChoiceField(
        choices=S,
        label="Sexo",
        widget=forms.Select(attrs=listF('placeholder', '')))

    # email = forms.CharField(widget=forms.EmailInput(),required=False)
    password1 = forms.CharField(widget=forms.HiddenInput(), required=False)
    password2 = forms.CharField(widget=forms.HiddenInput(), required=False)
    username = forms.CharField(widget=forms.HiddenInput(), required=False)

    class Meta:
        model = Users
        fields = ('names', 'first_surname', 'second_surname', 'username', 'dni', 'sex', 'birth_date')
        # exclude = ('password1','password1','email',)

        widgets = {
            'names': forms.TextInput(attrs=listF('placeholder', 'ingrese nombres completos')),
            'first_surname': forms.TextInput(attrs=listF('placeholder', 'ingrese apellido paterno')),
            'second_surname': forms.TextInput(attrs=listF('placeholder', 'ingrese apellido materno')),
            'birth_date': forms.DateInput(format=('%Y-%m-%d'), attrs={'type': 'date'}),
            'dni': forms.TextInput(attrs=listF('placeholder', 'ingrese DNI')),
        }

    def __init__(self, *args, **kwargs):
        super(CustomerCreationForm, self).__init__(*args, **kwargs)
        self.fields.pop('username')
        # self.fields.pop('email')
        self.fields.pop('password1')
        self.fields.pop('password2')


class LoginForm(AuthenticationForm):
    username = forms.CharField(label="Usuario", required=True, widget=forms.TextInput(
        attrs={'class': "validate  www", 'autofocus': ''}
    ))
    password = forms.CharField(label="Contraseña", required=True, widget=forms.PasswordInput(
        attrs={'class': "validate www"}
    ))

    error_messages = {
        'invalid_login': ("Por favor verifique %(username)s o contraseña. "),
        'inactive': ("Cuenta desactivada."),
    }
    def clean(self):
        username = self.cleaned_data.get('username')
        password = self.cleaned_data.get('password')

        if username and password:
            self.user_cache = authenticate(username=username, password=password)
            if self.user_cache is None:
                raise forms.ValidationError(
                    self.error_messages['invalid_login'],
                    code='invalid_login',
                    params={'username': self.username_field.verbose_name},
                )
            else:
                self.confirm_login_allowed(self.user_cache)

        return self.cleaned_data


class UserCreationForm(forms.ModelForm):
    """A form for creating new Users_Core. Includes all the required
    fields, plus a repeated password."""
    password1 = forms.CharField(label='Contraseña', widget=forms.PasswordInput)
    password2 = forms.CharField(label='Confirmar Contraseña', widget=forms.PasswordInput)

    class Meta:
        model = Users
        fields = ('username', 'email', 'dni')
        d = {'class': 'form-control input-sm'}
        widgets = {
            'username': forms.TextInput(attrs=d),
            'email': forms.EmailInput(attrs=d),
            'password': forms.PasswordInput(attrs=d),
            'dni': forms.TextInput(attrs=d),
        }

    def clean_password2(self):
        # Check that the two password entries match
        password1 = self.cleaned_data.get("password1")
        password2 = self.cleaned_data.get("password2")
        if password1 and password2 and password1 != password2:
            raise forms.ValidationError("Contraseñas no coincides")
        return password2

    def save(self, commit=True):
        # Save the provided password in hashed format
        user = super(UserCreationForm, self).save(commit=False)
        user.set_password(self.cleaned_data["password1"])
        if commit:
            user.save()
        return user


class UserChangeForm(forms.ModelForm):
    class Meta:
        model = Users
        fields = 'username', 'email', 'password', 'dni'

    def clean_password(self):
        return self.initial["password"]


class profileForm(forms.ModelForm):
    permissions = forms.ModelMultipleChoiceField(
        queryset=permission.objects.filter(status=True),
        label="Permisos",
        widget=forms.SelectMultiple())

    class Meta():
        model = profile
        fields = '__all__'
        exclude = ('status', 'deleted_at')

        widgets = {
            'description': forms.TextInput(),
        }

        error_messages = {
            "permissions": {
                'required': ("escoja un permiso"),
            },
        }


class permissionForm(forms.ModelForm):
    idmodulo = forms.ModelChoiceField(
        queryset=modules.objects.filter(status=True).order_by('id'),
        label="Módulos",
        widget=forms.Select())

    class Meta():
        model = permission
        fields = '__all__'
        exclude = ('status', 'deleted_at')

        widgets = {
            "description": forms.TextInput(),
        }

        error_messages = {
            "module": {
                'required': ("escoja un módulo"),
            },
        }


class moduleAdd(forms.ModelForm):
    class Meta():
        model = modules
        fields = 'description', 'icon'
        exclude = ('status',)
        widgets = {
            'description': forms.TextInput(attrs={'onkeyup': '$("#url_m").val($(this).val()+"/"+$(this).val())'}),
            'icon': forms.TextInput(attrs={}),
            'url': forms.HiddenInput()
        }


class submoduleAdd(forms.ModelForm):
    class Meta():
        model = modules
        fields = 'description', 'icon', 'url', 'father'
        exclude = ('status',)


class cueform(forms.ModelForm):
    birth_date = forms.DateField(required=False, label='Fecha de nacimiento',
                                 widget=forms.DateInput(format=('%Y-%m-%d'),
                                                        attrs={'placeholder': 'ingrese fecha de nacimiento',
                                                               'class': 'form-control input-sm', 'type': 'date'}
                                                        ))
    cellphone = forms.CharField(required=False, label='Celular',
                                widget=forms.TextInput(
                                    attrs={'placeholder': 'ingrese celular',
                                           'class': 'form-control input-sm'}))
    telephone = forms.CharField(required=False, label='Teléfono',
                                widget=forms.TextInput(
                                    attrs={'placeholder': 'ingrese Teléfono Fijo/ u otro',
                                           'class': 'form-control input-sm'}))
    address = forms.CharField(required=False, label='Dirección',
                              widget=forms.TextInput(
                                  attrs={'placeholder': 'ingrese Dirección de su casa',
                                         'class': 'form-control input-sm'}))

    class Meta():
        model = Users
        fields = ('__all__')
        widgets = {
            'username': forms.TextInput(attrs={'placeholder': 'ingrese usuario', 'class': 'form-control input-sm'}),
            'names': forms.TextInput(
                attrs={'placeholder': 'ingrese nombres completos', 'class': 'form-control input-sm'}),
            'first_surname': forms.TextInput(
                attrs={'placeholder': 'ingrese apellido paterno', 'class': 'form-control input-sm'}),
            'second_surname': forms.TextInput(
                attrs={'placeholder': 'ingrese apellido materno', 'class': 'form-control input-sm'}),
            'email': forms.EmailInput(attrs={'placeholder': 'ingrese email', 'class': 'form-control input-sm'}),
            'Sex': forms.Select(attrs={'class': 'form-control input-sm'}),
        }
        exclude = ('password', 'foto', 'foto_url', 'dni', 'is_staff', 'is_active', 'is_admin', 'social_netwok',
                   'last_login', 'blood_group', 'work_experiencie', 'date_joined', 'password_default', 'status',
                   'deleted_at',
                   'is_superuser', 'groups', 'user_permissions', 'is_staff', 'social_network', 'profiles')

        labels = {
            'sex': 'Sexo',
        }


class formFoto(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(formFoto, self).__init__(*args, **kwargs)
        self.fields['foto'].widget = forms.FileInput()

    class Meta:
        model = Users
        exclude = (
            'password', 'foto_url', 'dni', 'telephone', 'cellphone', 'address', 'is_staff', 'is_active', 'is_admin',
            'status', 'deleted_at', 'is_superuser', 'groups', 'user_permissions', 'profiles',
            'last_login', 'blood_group', 'work_experiencie', 'date_joined', 'password_default', 'sex', 'birth_date',
            'username',
            'names', 'first_surname', 'second_surname', 'email','civil_status','institucion', "permissions", 'social_netwok')


class UsuarioCreationForm(forms.ModelForm):
    password1 = forms.CharField(label=("Password"), widget=forms.PasswordInput)
    password2 = forms.CharField(label=("Password confirmation"), widget=forms.PasswordInput,
                                help_text=("Enter the same password as before, for verification."))

    email = forms.EmailField(
        help_text=('Email'),
        required=True,
        widget=forms.EmailInput(
            attrs={'class': 'form-control input-sm', 'placeholder': 'ingresar dirección de correo Electrónico'}),
    )

    foto = forms.ImageField(
        required=False,
        widget=forms.ClearableFileInput()
    )

    sex = forms.ChoiceField(
        choices=S,
        label="Sexo",
        widget=forms.Select(attrs=listF('placeholder', '')))

    profiles = forms.ModelMultipleChoiceField(
        queryset=profile.objects.filter(status=True),
        label="Tipo de Usuario",
        widget=forms.SelectMultiple(attrs=listF('placeholder', '')))

    email = forms.CharField(widget=forms.EmailInput(attrs=listF('placeholder', 'ingrese Email')), required=False)
    password1 = forms.CharField(widget=forms.HiddenInput(), required=False)
    password2 = forms.CharField(widget=forms.HiddenInput(), required=False)
    username = forms.CharField(widget=forms.HiddenInput(), required=False)
    address = forms.CharField(label='Dirección',
                              widget=forms.TextInput(attrs=listF('placeholder', 'ingrese Dirección')), required=False)
    cellphone = forms.CharField(label='Celular', widget=forms.TextInput(
        attrs={'placeholder': 'ingrese Celular', 'class': 'form-control input-sm',
               'max': 10, 'maxlength': 10
               },
    ),
    required=False)
    telephone = forms.CharField(label='Teléfono', widget=forms.TextInput(
        attrs={'placeholder': 'ingrese Teléfono', 'class': 'form-control input-sm',
               'max': 10, 'maxlength': 10
               },
    ), required=False)
    birth_date = forms.DateField(label='Fecha de Nacimiento', widget=forms.DateInput(format=('%Y-%m-%d'),
                                                                                     attrs={'type': 'date',
                                                                                            'placeholder': 'dd/mm/aaaa',
                                                                                            'class': 'form-control input-sm'}),
                                 required=False)

    class Meta:
        model = Users
        fields = ('names', 'first_surname', 'second_surname', 'username', 'dni', 'sex', 'foto', 'address',
                  'birth_date', 'cellphone', 'telephone', 'email')
        # exclude = ('password1','password1','email',)

        widgets = {
            'email': forms.EmailInput(attrs=listF('placeholder', 'ingrese Email')),
            'names': forms.TextInput(attrs=listF('placeholder', 'ingrese nombres completos')),
            'first_surname': forms.TextInput(attrs=listF('placeholder', 'ingrese apellido paterno')),
            'second_surname': forms.TextInput(attrs=listF('placeholder', 'ingrese apellido materno')),
            'dni': forms.TextInput(attrs=listF('placeholder', 'ingrese DNI')),
        }

    def __init__(self, *args, **kwargs):
        super(UsuarioCreationForm, self).__init__(*args, **kwargs)
        self.fields.pop('username')
        # self.fields.pop('email')
        self.fields.pop('password1')
        self.fields.pop('password2')

        # Only in case we build the form from an instance
        # (otherwise, 'profiles' list should be empty)
        if kwargs.get('instance'):
            # We get the 'initial' keyword argument or initialize it
            # as a dict if it didn't exist.
            initial = kwargs.setdefault('initial', {})
            # The widget for a ModelMultipleChoiceField expects
            # a list of primary key for the selected data.
            initial['profiles'] = [t.pk for t in kwargs['instance'].profiles.all()]

        forms.ModelForm.__init__(self, *args, **kwargs)

    # Overriding save allows us to process the value of 'profiles' field
    def save(self, commit=True):
        # Get the unsave Pizza instance
        instance = forms.ModelForm.save(self, False)

        # Prepare a 'save_m2m' method for the form,
        old_save_m2m = self.save_m2m

        def save_m2m():
            old_save_m2m()
            instance.profiles.clear()
            for p in self.cleaned_data['profiles']:
                instance.profiles.add(p)

        self.save_m2m = save_m2m

        # Do we need to save all changes now?
        if commit:
            instance.save()
            self.save_m2m()

        return instance


P = (
    ('2', 'Alumno'),
    ('3', 'Administrativo'),
    ('4', 'Docente'),
)


class UsersRegisterForm(forms.ModelForm):
    password1 = forms.CharField(label=("Contraseña"), strip=False, widget=forms.PasswordInput(
        attrs={'class': 'form-control input-sm', 'placeholder': 'ingresar contraseña'}))
    password2 = forms.CharField(label=("Confirmar Contraseña"),
                                strip=False,
                                widget=forms.PasswordInput(
                                    attrs={'class': 'form-control input-sm', 'placeholder': 'confirmar contraseña'}),
                                help_text=("Introduzca la misma contraseña, para su verificación."),

                                )

    email = forms.EmailField(
        help_text=('Email'),
        required=True,
        widget=forms.EmailInput(
            attrs={'class': 'form-control input-sm', 'placeholder': 'ingresar dirección de correo Electrónico'}),
    )

    profiles = forms.CharField(widget=forms.HiddenInput(attrs={'value': 2}))

    sex = forms.ChoiceField(
        choices=S,
        label="Sexo",
        widget=forms.Select(attrs=listF('placeholder', '')))

    class Meta:
        model = Users
        fields = ('names', 'first_surname', 'second_surname', 'username', 'dni', 'sex')
        # exclude = ('password1','password1','email',)

        widgets = {
            'username': forms.TextInput(attrs=listF('placeholder', 'ingrese Usuario')),
            'names': forms.TextInput(attrs=listF('placeholder', 'ingrese nombres completos')),
            'first_surname': forms.TextInput(attrs=listF('placeholder', 'ingrese apellido paterno')),
            'second_surname': forms.TextInput(attrs=listF('placeholder', 'ingrese apellido materno')),
            'dni': forms.TextInput(attrs=listF('placeholder', 'ingrese DNI')),
        }

    def __init__(self, *args, **kwargs):
        super(Users_CoreRegisterForm, self).__init__(*args, **kwargs)

        # Only in case we build the form from an instance
        # (otherwise, 'profiles' list should be empty)
        if kwargs.get('instance'):
            # We get the 'initial' keyword argument or initialize it
            # as a dict if it didn't exist.
            initial = kwargs.setdefault('initial', {})
            # The widget for a ModelMultipleChoiceField expects
            # a list of primary key for the selected data.
            initial['profiles'] = [t.pk for t in kwargs['instance'].profiles.all()]

        forms.ModelForm.__init__(self, *args, **kwargs)

    def clean_password2(self):
        # Check that the two password entries match
        password1 = self.cleaned_data.get("password1")
        password2 = self.cleaned_data.get("password2")
        if password1 and password2 and password1 != password2:
            raise forms.ValidationError("Contraseñas no coinciden")
        self.instance.username = self.cleaned_data.get('username')
        password_validation.validate_password(self.cleaned_data.get('password2'), self.instance)

        return password2

    def clean(self):
        username = self.cleaned_data.get('username')
        password = self.cleaned_data.get('password')

        if username and password:
            self.user_cache = authenticate(username=username,
                                           password=password)
            if self.user_cache is None:
                raise forms.ValidationError(
                    self.error_messages['invalid_login'],
                    code='invalid_login',
                    params={'username': self.username_field.verbose_name},
                )
            else:
                self.confirm_login_allowed(self.user_cache)

        return self.cleaned_data

    def confirm_login_allowed(self, user):
        """
        Controls whether the given User may log in. This is a policy setting,
        independent of end-user authentication. This default behavior is to
        allow login by active Users_Core, and reject login by inactive Users_Core.

        If the given user cannot log in, this method should raise a
        ``forms.ValidationError``.

        If the given user may log in, this method should return None.
        """
        if not user.is_active:
            raise forms.ValidationError(
                self.error_messages['inactive'],
                code='inactive',
            )

    def get_user_id(self):
        if self.user_cache:
            return self.user_cache.id
        return None

    def get_user(self):
        return self.user_cache

    # Overriding save allows us to process the value of 'profiles' field
    def save(self, commit=True):
        # Get the unsave Pizza instance
        instance = forms.ModelForm.save(self, False)

        # Prepare a 'save_m2m' method for the form,
        old_save_m2m = self.save_m2m

        def save_m2m():
            old_save_m2m()
            instance.profiles.clear()
            for p in self.cleaned_data['profiles']:
                instance.profiles.add(p)

        self.save_m2m = save_m2m

        # Do we need to save all changes now?
        if commit:
            instance.save()
            self.save_m2m()

        return instance


'''
class formcontacto(forms.ModelForm):
    class Meta():
        model = contacto
        fields = '__all__'
        exclude = ('status', 'deleted_at')
        widgets = {
            "name": forms.TextInput(),
            'email': forms.EmailInput(),
            'telefono': forms.NumberInput(),
            'message': forms.Textarea(attrs={'class': 'materialize-textarea'}),
        }
'''