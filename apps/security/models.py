from django.contrib.auth.models import (
    BaseUserManager, AbstractBaseUser, PermissionsMixin
)
from django.core import validators
from django.db import models
from django.utils import timezone
from django.utils.safestring import mark_safe

from apps.security.core.core import TimeStampedModel


class icons(models.Model):
    description = models.CharField(max_length=100)
    html_class = models.CharField(max_length=100)
    def __str__(self):
        return mark_safe("<li class='" + self.html_class + "'><li> " + self.description)

class modules(TimeStampedModel):
    description = models.CharField(max_length=50)
    father = models.IntegerField(default=0)
    url = models.CharField(max_length=150)
    icon = models.ForeignKey(icons, null=True, related_name='pk_icon', on_delete=models.CASCADE)
    order_mod = models.IntegerField(null=True)
    order_submod = models.IntegerField(null=True)
    def __str__(self):
        return self.description

class permission(TimeStampedModel):
    description = models.CharField(max_length=50)
    module    = models.ForeignKey(modules, on_delete=models.CASCADE)

    def __str__(self):
        return self.description

    def list_permisos(l):
        l['status'] = True
        d = permission.objects.filter(**l)
        return d


class profile(TimeStampedModel):
    description = models.CharField(max_length=50)
    permissions = models.ManyToManyField(permission, related_name="detail_profile_permisssion")

    def __str__(self):
        return self.description

    def permission_module(r, self):
        mod = modules.objects.filter(status=True)
        per = permission.objects.filter(status=True)

        if r.GET.get('id') == "":
            pp = ''
        else:
            pp = profile.objects.filter(pk = r.GET.get('id'), status=True ).values('permissions__description','permissions__module_id')
            pl = []
            for l in pp:
                if l["permissions__module_id"] is not None:
                    pl.append(str(l["permissions__module_id"])+"-"+l["permissions__description"])
        c = []
        for m in mod:
            if m.father == 0:
                modid = m.id
                b = False
                for o in per:
                    if modid == o.module_id:
                        if pp:
                            se = str(str(o.module_id)+"-"+ o.description) in pl
                        else:
                            se = True
                        f = {'id': o.id, 'nombre': o.description, 'selected': se}
                        b = True
                if not b:
                    f = None

                h = []
                for sb in mod:
                    if modid == sb.father:
                        sbid = sb.id
                        pm = []
                        for p in per:
                            if sbid == p.module_id:
                                if pp:
                                    se = str(str(p.module_id) + "-" + p.description) in pl
                                else:
                                    se = True
                                pm.append({'id':p.id,'nombre':p.description, 'selected':se})

                        h.append({'id':sb.id,'nombre': sb.description, 'permisos':pm})
                c.append({"padre": m.description,'id':m.id,'idpadre': f, "hijos": h})
        return  c

class MyUserManager(BaseUserManager):
    def create_user(self, email, username, dni, password=None):
        if not email:
            raise ValueError('El usuario tiene una dirección email incorrecta')

        if not dni:
            raise ValueError('El usuario tiene dni incorrecta')

        user = self.model(
            email    = self.normalize_email(email),
            username = username,
            dni      = dni,
        )

        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, email, username, dni, password):
        
        user = self.create_user(email,
            password = password,
            username = username,
            dni      = dni
        )
        user.is_admin = True
        user.save(using=self._db)
        return user

class ubigeo(TimeStampedModel):
    departament = models.IntegerField(null=True, blank=True)
    province    = models.IntegerField(null=True, blank=True)
    distrit     = models.IntegerField(null=True, blank=True)
    name        = models.CharField(max_length=100)

    # this is for query of departament
    def c_departament(self):
        d = ubigeo.objects.filter(distrit=0, province=0).values('departament','name')
        return d

    # query of province by departament
    def c_provincia(iddepa):
        p = ubigeo.objects.filter(departament=iddepa, distrit=0).exclude(province=0).values('name','province')
        return  p

    #query of distrit by province and departament
    def c_distrit(iddepa,idprov):
        di = ubigeo.objects.filter(departament=iddepa, province=idprov).exclude(distrit=0).values('id','name')
        return di


sc = (
    ('soltero','soltero'),
    ('casado','casado'),
    ('divorciado','divorciado'),
    ('viudo','viudo'),
)

class Users_Core(AbstractBaseUser, PermissionsMixin, TimeStampedModel, ):
    ubigeo          = models.ForeignKey(ubigeo, blank=True, null=True, on_delete=models.CASCADE)
    group_blood     = models.CharField(max_length=5, blank=True, null=True)
    ruc             = models.CharField(max_length=13, blank=True, null=True)
    civil_status    = models.CharField(max_length=15,choices=sc, default="Soltero")

    username = models.CharField(
        ('Usuario'),
        max_length  = 30,
        unique      = True,
        help_text   = ('Requiere: 30 carácteres o menos. Letras, digitos y  @/./+/-/_ sólo.'),
        validators  = [
            validators.RegexValidator(
                r'^[\w.@+-]+$',
               ('Ingrese un usuario válido. Este valor solo podrá contener '
                 'Letras, Números ' 'and @/./+/-/_ Carácteres.')
            )
        ],
        error_messages={
            'unique': ("Ya existe usuario con ese nombre"),
        },
    )
    names            = models.CharField(max_length=50,null=True, verbose_name='Nombres')
    first_surname    = models.CharField(max_length=50,null=True, verbose_name='Apellido Paterno')
    second_surname   = models.CharField(max_length=50,null=True, verbose_name='Apellido Materno')
    dni              = models.CharField(
        max_length=8,
        unique=True,
        help_text=('DNI Requiere: 8 carácteres obligatorios'),
        validators=[
            validators.RegexValidator(
                r'^[\w.@+-]+$',
                ('Ingrese un dni válido. Este valor solo podrá contener '
                 'Números' 'de 8 Dígitos.')
            ),
        ],
        error_messages={
            'unique': ("Ya existe dni, probar con otro"),
        },
    )
    Sex = (
        ('f', 'Femenino'),
        ('m', 'Masculino'),
    )

    email            = models.EmailField(verbose_name='Dirección de email',max_length=255,  blank=True, null=True)
    birth_date       = models.DateField(null=True, verbose_name='Fecha de Nacimiento')
    foto             = models.ImageField(max_length=500, upload_to='fotoperfil', null=True)
    cellphone        = models.CharField(max_length=100, null=True, verbose_name='Celular')
    telephone        = models.CharField(max_length=100, null=True)
    address          = models.CharField(max_length=200,null=True)
    work_experiencie = models.CharField(max_length=30, null=True)
    sex              = models.CharField(max_length=1, choices=Sex)
    password_default = models.CharField(max_length=15, null=True)

    profiles = models.ManyToManyField(profile, blank=True)
    permissions = models.ManyToManyField(permission, blank=True)
    is_staff = models.BooleanField(
        ('staff status'),
        default=False,
        help_text=('Indica si el usuario puede iniciar sesión en este sitio de administración.'),
    )
    is_active = models.BooleanField(
        ('active'),
        default=True,
        help_text=(
            'Designa si el usuario debe ser tratado como activo. '
            'Seleccionarla en lugar de eliminar las cuentas .'
        ),
    )
    date_joined = models.DateTimeField(('fecha de Inscripción'), default=timezone.now)
    is_admin         = models.BooleanField(default=False)

    USERNAME_FIELD   = 'username'
    REQUIRED_FIELDS  = ['dni','email']

    objects = MyUserManager()

    class Meta:
        abstract = True

    def get_full_name(self):
        # The user is identified by their email address
        return self.email



    def get_short_name(self):
        # The user is identified by their email address
        return self.username

    def __str__(self):              # __unicode__ on Python 2
        return str(str(self.dni)+" "+self.first_surname.capitalize() + " " +  self.second_surname.capitalize() + ", " + self.names.capitalize())

    def current_age(born):
        if born is None:
            return None
        today = timezone.now().date()
        return today.year - born.year - ((today.month, today.day) < (born.month, born.day))

    def has_perm(self, perm, obj=None):
        "Does the user have a specific permission?"
        # Simplest possible answer: Yes, always
        return True

    def has_module_perms(self, app_label):
        "Does the user have permissions to view the app `app_label`?"
        # Simplest possible answer: Yes, always
        return True

    @property
    def is_staff(self):
        "Is the user a member of staff?"
        # Simplest possible answer: All admins are staff
        return self.is_admin

class SystemError(TimeStampedModel):
    description = models.TextField()
    user = models.IntegerField()
    url = models.TextField()
    funtion = models.CharField(max_length=100)