import json

from apps.security.core.modelos import get_li
from django.contrib.auth.decorators import login_required
from django.core.serializers.json import DjangoJSONEncoder
from django.db.models import Q
from django.http import HttpResponseBadRequest
from django.shortcuts import render, get_object_or_404, HttpResponse

from apps.security.core.manipulate import create_app, create_crud, read_write
from apps.security.forms import moduleAdd, submoduleAdd
from apps.security.models import icons, modules, permission


def error(f):
    ee = {}
    for i in f.errors:
        e = f.errors[i]
        ee[i] = str(e)
    return HttpResponseBadRequest(json.dumps(ee))

@login_required(login_url='login/')
def module(m):
    f  = moduleAdd()
    if m.method == 'POST':
        f = moduleAdd(m.POST)
        if f.is_valid():
            s = f.save()
            if m.POST.get('app') == "on":
                create_app(s.description)
            p = permission()
            p.idmodulo_id = s.id
            p.descripcion = "listar"
            p.save()
            return HttpResponse("módulo creado correctamente")
        else:
            return error(f)

    elif m.GET.get('type')=="json":
        o = modules.objects.filter(status = True).values(
            'id','description','icon__description','icon__html_class','father'
        ).order_by('-id')
        if m.GET.get('id'):
            o = o.filter(father=int(m.GET.get('id')))

        return HttpResponse(json.dumps(list(o)))

    o = modules.objects.select_related('icon').filter(status = True).values(
        'id','description','url','father',
        'icon__html_class'
        )
    return render(m, 'system/module/t_module.html', {'module': o, 'formulario': f })

@login_required(login_url='login/')
def deletemodule(em, id):
    e = modules.objects.get(pk = id)
    e.status = False
    e.save()
    return HttpResponse(e.father)


@login_required(login_url='login/')
def updatemodule(ed, id):
    if ed.GET.get("type")=='json':
        su = modules.objects.filter(status = True,id = id)
        return HttpResponse(json.dumps(list(su.values('id','description','icon__description','icon__html_class','icon_id','father')
                                            ),cls=DjangoJSONEncoder))
    if ed.method == 'POST':
        a=get_object_or_404(modules,pk=id)
        if  ed.POST['mod']:
            f = moduleAdd(ed.POST, instance=a)
        else:
            f = submoduleAdd(ed.POST, instance=a)

        if f.is_valid():
           f.save()
        else:
            return error(f)
    return HttpResponse('DATOS EDITADOS')


@login_required(login_url='login/')
def editamh(r):
    idm = r.GET.get("idmh")
    su = modules.objects.filter(status=True, id=idm)
    return HttpResponse(
        json.dumps(list(su.values('id','url', 'description', 'icon__description', 'icon__html_class', 'icon_id', 'father')
                        ), cls=DjangoJSONEncoder))



import os

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
@login_required(login_url='login/')
def submodule(sub):
    if sub.method == 'POST':
        f = submoduleAdd(sub.POST)
        if f.is_valid():
            f = f.save()
            if sub.POST.get('crud'):
                app = sub.POST.get("modulo")
                name = f.description
                uf = BASE_DIR + "/../../config/"
                read_write(uf + "modelos.py", "#.-", "#-","from apps."+app+".models import "+name+"\n")
                for i in ["listar",'add','update','delete']:
                    p = permission()
                    p.idmodulo_id = f.id
                    p.descripcion = i
                    p.save()

            return HttpResponse("Guardado correctamente")
        else:
            return error(f)

@login_required(login_url='login/')
def v_create_crud(r):
    app = r.GET.get("app")
    name = r.GET.get("name")
    li = get_li(name)
    create_crud(app,name,li)
    return HttpResponse("ok")

@login_required(login_url='login/')
def json_submod(r):
    su = modules.objects.select_related('icon').filter(status = True, father = r.GET.get('id')).values('id',
        'description','icon__html_class')
    data = json.dumps(list(su))
    return HttpResponse(data)

@login_required(login_url='login/')
def fonts(r):
    t  = r.GET.get("q")
    pg = r.GET.get("page")

    if t is not None:
        p = icons.objects.filter( Q(description__contains=t) ).values()[:pg]
    else:
        p = icons.objects.values()[:pg]

    return HttpResponse(json.dumps(list(p),cls=DjangoJSONEncoder))

