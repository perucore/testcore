from registration.backends.hmac.views import RegistrationView
from django.http import HttpResponseBadRequest,HttpResponse
from django.contrib.auth import authenticate, login, logout
from django.contrib import auth
from django.http import HttpResponseRedirect
from django.views.generic import FormView
from django.urls import reverse_lazy
from django.contrib.auth import get_user_model
Users = get_user_model()
from django.shortcuts import render
from django.db import connection
#from apps.capturaMac.direccionip import dirrecionip,validaduser
from apps.security.forms import LoginForm, UsersRegisterForm

from apps.security.forms import LoginForm, UsersRegisterForm
import json


def dictfetchall(cursor):
    "Return all rows from a cursor as a dict"
    columns = [col[0] for col in cursor.description]
    return [
        dict(zip(columns, row))
        for row in cursor.fetchall()
        ]

class login(FormView):
    form_class  = LoginForm
    template_name = "login/login.html"
    success_url = reverse_lazy('index')

    def post(self, request, *args, **kwargs):
        username = self.request.POST['username']
        idI = self.request.session['institucion']['id'] #id de Institucion
        ie = Users.__validate_users_in_institucion__(username=username, institucion=idI)  # validate users in Institucion
        if ie:
            form = self.get_form()
            if form.is_valid():
                return self.form_valid(form)
            else:
                return self.form_invalid(form)
        else:
            return HttpResponseBadRequest(json.dumps({'__all__':"No estas Registrado"}))

    def get_context_data(self, **kwargs):
        context = super(login, self).get_context_data(**kwargs)
        return context

    def dispatch(self, request, *args, **kwargs):
        if request.user.is_authenticated:
            return HttpResponseRedirect(self.get_success_url())
        else:
            return super(login, self).dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        username = self.request.POST['username']
        password = self.request.POST['password']
        user = authenticate(username=username, password=password)
        auth.login(self.request, user)
        #dirrecionip(self.request)
        return super(login, self).form_valid(form)


    def form_invalid(self, form):
        ee = {}
        if form.errors:
            for i in form.errors:
                e = form.errors[i]
                ee[i] = str(e)
        return HttpResponseBadRequest(json.dumps(ee))

def LogOut(request):
    logout(request)
    return HttpResponseRedirect('/')

class register(RegistrationView):
    form_class  = UsersRegisterForm
    template_name = "form_regis_user.html"

    def get_success_url(self, user):
        return ('/security/usuarios/completado', (), {})

    def form_invalid(self, form):
        ee = {}
        if form.errors:
            for i in form.errors:
                e = form.errors[i]
                ee[i] = str(e)
        return HttpResponseBadRequest(json.dumps(ee))

    def create_inactive_user(self, form):

        new_user                   = form.save(commit=False)
        new_user.is_active         = True
        new_user.username          = form.cleaned_data["username"]
        new_user.email             = form.cleaned_data["email"]
        new_user.password_default  = form.cleaned_data["password2"]
        new_user.set_password(form.cleaned_data["password2"])
        new_user.save()

        for i in self.request.POST.getlist('profiles'):
            new_user.profiles.add(int(i))

        return new_user