from braces.views import LoginRequiredMixin
from django.contrib.auth.decorators import login_required
from django.urls import reverse_lazy
from django.http import HttpResponseRedirect
from django.shortcuts import HttpResponse
from django.views.generic import TemplateView
from django.views.generic.edit import FormView, UpdateView

from apps.security.core import error
from apps.security.forms import profileForm
from apps.security.models import profile


class TemplateProfile(LoginRequiredMixin,TemplateView):
    login_url = reverse_lazy('login')
    template_name = 'system/profile/t_profile.html'

import json
@login_required(login_url='login/')
def list_profiles(r):
    data = profile.objects.values('id','description').filter(status=True)
    return HttpResponse(json.dumps(list(data)))



@login_required(login_url='login/')
def list_profiles_modules(r):
    data = profile.permission_module(r,"p")
    return HttpResponse(json.dumps(list(data)))


class SaveBase(LoginRequiredMixin, FormView):
        login_url = reverse_lazy('login')
        form_class = profileForm
        template_name = "system/profile/frm_profile.html"
        success_url = reverse_lazy('ok')

        def get_context_data(self, **kwargs):
            context = super(SaveBase, self).get_context_data(**kwargs)
            context['name'] = 'Registrar'
            return context

        def form_valid(self, form):
            form.save()
            return HttpResponseRedirect(self.get_success_url())

        def form_invalid(self, form):
            return error(form)


class UpdateBase(LoginRequiredMixin, UpdateView):
    login_url = reverse_lazy('login')
    form_class = profileForm
    template_name = "system/profile/frm_profile.html"
    success_url = reverse_lazy('ok')
    model = profile

    def get_context_data(self, **kwargs):
        context = super(UpdateBase, self).get_context_data(**kwargs)
        context['name'] = 'Actualizar'
        return context

    def form_valid(self, form):
        form.save()
        return HttpResponseRedirect(self.get_success_url())

    def form_invalid(self, form):
        return error(form)



@login_required(login_url='login/')
def deleteprofile(em, id):
    dp = profile.objects.get(pk=id)
    dp.status = False
    dp.save()

    return HttpResponse('DATOS ELIMINADOS')



