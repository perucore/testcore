from django.contrib.auth import get_user_model
from django.core.mail import send_mail
from django.shortcuts import render, get_object_or_404, HttpResponse

from apps.security.forms import UserChangeForm, formFoto

Users = get_user_model()
from apps.security.models import modules
from apps.security.core.pagination.num_pag import paginacion

from django.db.models import Count
def system(r):
    u = r.user.profiles
    template_name = 'system/index.html'
    if len(u.filter(status=True)) > 1:
        idprofile = r.GET.get("sys")

        if idprofile is None:
            p = {'profile':u.filter(status=True)}
            template_name = 'system/system.html'
            #template_name = 'system/systemprueba.html'
            return render(r,template_name,p)

    else:
        idprofile = u.filter(status=True)[0].id

    queryset1 = u.filter(pk = idprofile, status=True, permissions__status=True, permissions__module__status=True).values(
        'permissions__module__description', 'permissions__module__url', 'permissions__module__father',
        'permissions__module__icon__html_class', 'permissions__module_id','id', 'description'
    ).annotate(dcount=Count('permissions__module__description')).order_by('permissions__module__order_mod', 'permissions__module__order_submod')

    queryset2 = r.user.permissions.values(
        'module__description', 'module__url', 'module__father',
        'module__icon__html_class', 'module_id'
    ).filter(module__status=True)

    # union de las dos consultas identificando los datos de la lista iguales para no agregarlo
    modulos = []
    for datoquery1 in queryset1:
        if datoquery1 not in modulos:
            modulos.append(datoquery1)
    for datoquery in queryset2:
        if datoquery not in modulos:
            modulos.append(datoquery)

    profile_des = modulos[0]['description'] if modulos else ''
    p = {'modulo':modulos, 'profile':u.filter(status=True),'profile_des':profile_des}
    return render(r, template_name, p)

def ok(o):
    return HttpResponse("ok")


#@login_required(login_url='login/')
def index(r):
    if r.user.is_authenticated:
        return system(r)
    else:
        template_name = "Institucion/index.html"
        p = {}
        return render(r, template_name, p)


def demo(r):
    if r.method == "POST":
        filehandle = r.FILES['file']
        di = dict(filehandle.get_dict())

        for ic in di.keys():
            li = []
            for id in range(len(di[ic])):
                dd = {}
                for ic in di.keys():
                    dd[ic] = di[ic][id]
                li.append(dd)
            break

        li = [li[i:i + 21] for i in range(0, len(li), 21)]

        return render(r,'diseño.html',{'lista':li})
    else:
        return render(r, "prueba.html", {})

def restauraContraseña(r):

    return HttpResponse('hola')
def hacerrr(r):
    for i in r.GET:
       j=i[0]

    return render(r,"newcontraseña.html",{'identificador':j})
def confirmacionemail(r):

    u=Users.objects.filter(email=r.POST.get('email'))
    for j in u:
        print(j)
    send_mail(
        'Empresa PeruCore le da la bienvenida!',
        'Gracias por contactarse con nosotros,  http://127.0.0.1:8000/restorecontrase%C3%B1a/?1',
        'perucoresoft@gmail.com',
        [r.POST.get('email')],
        fail_silently=False,
    )

    return HttpResponse ("llego")
def dictfetchall(cursor):
        "Return all rows from a cursor as a dict"
        columns = [col[0] for col in cursor.description]
        return [
            dict(zip(columns, row))
            for row in cursor.fetchall()
        ]

def lmodulos(m):
    return render(m, 'system/module/modules.html')


def fotoperfil(r):
    a     = get_object_or_404(Users, pk=r.user.id)
    formu = formFoto(r.POST, r.FILES, instance=a)
    if formu.is_valid():
        formu.save()
    return HttpResponse("ok")

def usuario(r):
    return render(r, "system/account/usuario.html")


def guardarpadre(r):
    f=UserChangeForm(r.POST)
    f.save()
    return HttpResponse("guardado correctamente")

def listar_prueba(r):
    template_name = "system/users/t_modules.html"
    u=modules.objects.filter(status=True).values()
    #u = Users.objects.filter(status=True)
    for fila in u:
        print(fila)
    l= {'search': "'modules','[\"description\",\"url\"]'"}
    v = {'values': "'None'",'template':"'datoAjaxModuless.html'",}
    l.update(v)
    return paginacion(r,u,"",template_name,l)



