from apps.security.forms import permissionForm
from apps.security.models import permission, modules
from apps.security.views.view_profile import SaveBase, UpdateBase
from braces.views import LoginRequiredMixin
from django.contrib.auth.decorators import login_required
from django.urls import reverse_lazy
from django.shortcuts import HttpResponse
from django.views.generic import ListView


class list_permission(LoginRequiredMixin, ListView ):
    login_url = reverse_lazy('login')

    template_name = "system/permission/list_permission.html"
    model = permission
    queryset = permission.objects.filter(status=True).values('id', 'description', 'module__description')

    def get_context_data(self, **kwargs):
        c = super(list_permission, self).get_context_data(**kwargs)
        module = modules.objects.filter(status=True)
        l = {}
        if self.request.GET.get('module'):
            l["module_id"] = self.request.GET.get('id_module')

        queryset = permission.list_permisos(l)

        c.update({ 'module': module,'object_list': queryset})

        return c


class add_permission(SaveBase):
    form_class = permissionForm
    template_name = "system/permission/frm_permission.html"

class update_permission(UpdateBase):
    form_class = permissionForm
    template_name = "system/permission/frm_permission.html"
    model = permission

@login_required(login_url='login/')
def deletepermission(em, id):
    dp = permission.objects.get(pk=id)
    dp.status = False
    dp.save()

    return HttpResponse('DATOS ELIMINADOS')

