from django.shortcuts import render,get_object_or_404, HttpResponse
from apps.security.models import users_profile_permission,profile,profile_permision
from django.contrib.auth.models import Permission
from rest_framework.response import Response
from django.db import models,connection
from django.contrib.contenttypes.models import ContentType
from apps.core.core import Core
from django.core import serializers
import json

def moduloperm(r):
    n = connection.cursor()
    n.execute('SELECT "public".django_content_type."id" as id_content,'+
              ' "public".django_content_type.app_label,'+
            ' "public".django_content_type.model,'+
            ' "public".auth_permission."id",'+
            ' "public".auth_permission."name",'+
            ' "public".auth_permission.content_type_id,'+
            ' "public".auth_permission.codename'+
              ' FROM'+
            ' "public".django_content_type'+
              ' INNER JOIN "public".auth_permission ON "public".auth_permission.content_type_id = "public".django_content_type."id" order by content_type_id  ');
    context = Core.querySql(n)

    modul=ContentType.objects.all()
    perm=["index", "actualizar", "eliminar", "importar", "exportar"]

    dictx={'objectx':context,
           'perm':perm,
           'modul':modul
           }

    return render(r, "modulperm.html", dictx)


def add_nuevos(r):

    perm = r.POST.get("permission")
    perm=int(perm)

    acc= r.POST.getlist("accion")
    for d in acc:
        insert_p = Permission()
        insert_p.name=d
        insert_p.content_type_id=perm
        insert_p.codename=d
        insert_p.save()
    p="Se guardo Correctamente"

    return HttpResponse(p)

def update_moduloperm(r):
    id=r.POST.get("id")
    id=int(id)

    permision=connection.cursor()
    permision.execute('SELECT  "public".auth_permission."id", "public".auth_permission."name","public".auth_permission.content_type_id,'+
                       ' "public".auth_permission.codename FROM "public".auth_permission '+
                       ' where "public".auth_permission.content_type_id = '+ str(id))
    Perm=Core.querySql(permision)

    perm = ["index", "actualizar", "eliminar", "importar", "exportar"]
    dicx=json.dumps(Perm)
    print(dicx)
    return HttpResponse(dicx)

def upd_moduloperm(r):

    id=r.POST.get("idxy")
    Permission.objects.filter(content_type_id=id).delete()
    perm = r.POST.get("permission")
    perm = int(perm)

    acc = r.POST.getlist("accion")
    for d in acc:
        insert_p = Permission()
        insert_p.name = d
        insert_p.content_type_id = perm
        insert_p.codename = d
        insert_p.save()
    p = "Se guardo Correctamente"
    return HttpResponse(p)

def update_userperm(r):
    id=r.POST.get("id")
    perm = connection.cursor()
    perm.execute('SELECT "public".security_users_profile_permission.users_profiles_id,'+
        ' "public".security_users_profile_permission.permission_id,"public".security_users_profile_permission.status,'+
        ' "public".security_users_profile_permission."id",  "public".auth_permission."name",'+
        ' "public".auth_permission.codename, "public".django_content_type.app_label,'+
        ' "public".django_content_type.model   FROM "public".security_users_profile_permission'+
        ' INNER JOIN "public".auth_permission ON "public".security_users_profile_permission.permission_id = "public".auth_permission."id"'+
        ' INNER JOIN "public".django_content_type ON "public".auth_permission.content_type_id = "public".django_content_type."id" where "public".security_users_profile_permission.users_profiles_id='+id)
    Perm = Core.querySql(perm)
    Perm=json.dumps(Perm)
    return HttpResponse(Perm)

def upd_userperm(r):

    users_profile_permission.objects.filter(users_profiles_id=r.POST.get("valx")).update(status=False)
    user_per = r.POST.get("user_p")
    user_per = int(user_per)

    perms = r.POST.getlist("perm")

    for d in perms:
        insert_p = users_profile_permission()
        insert_p.users_profiles_id = user_per
        insert_p.permission_id = d
        insert_p.save()
    p = "Se guardo Correctamente"
    return HttpResponse("ok")

def index_upp(r):

    permx=connection.cursor()
    permx.execute('SELECT  "public".django_content_type.app_label,'+
                ' "public".django_content_type.model,'+
                ' "public".auth_permission."id",'+
                ' "public".auth_permission."name",'+
                ' "public".auth_permission.content_type_id,'+
                  '  "public".auth_permission.codename'+
                  ' FROM'+
                ' "public".auth_permission'+
                  ' INNER JOIN "public".django_content_type ON "public".auth_permission.content_type_id = "public".django_content_type."id" ')
    Permx=Core.querySql(permx)

    #-------------
    userp=connection.cursor()
    userp.execute('SELECT "public".security_profile.description,'+
                  ' "public".security_users_profiles.users_id,'+

                  ' "public".security_users_profiles.profile_id,'+
                  ' "public".security_users_profiles."id",'+
                  ' "public".security_users.username,'+
                  ' "public".security_users.first_name,'+
                  ' "public".security_users.last_name,'+
                  ' "public".security_users.status,'+
                  ' "public".security_users.is_superuser'+
                  ' FROM '+
                  ' "public".security_users_profiles'+
                  ' INNER JOIN "public".security_users ON "public".security_users_profiles.users_id = "public".security_users."id"'+
                  ' INNER JOIN "public".security_profile ON "public".security_users_profiles.profile_id = "public".security_profile."id"   ')
    Userp=Core.querySql(userp)

    #-------
    n = connection.cursor()
    n.execute('SELECT "public".security_users_profiles.profile_id, "public".security_profile.description,'+
                ' "public".security_users.username,'+
                ' "public".security_users_profile_permission.users_profiles_id,'+
                ' "public".security_users_profile_permission.permission_id,'+
              ' "public".security_users_profile_permission.status,' +
                ' "public".auth_permission.codename,'+
                ' "public".auth_permission."name",'+
                ' "public".security_users."names",'+
                ' "public".security_users.first_name,'+
                ' "public".security_users.last_name,'+
                ' "public".auth_permission.content_type_id,'+
                ' "public".security_users_profiles.users_id'+
                ' FROM '+
                ' "public".security_users_profiles'+
                ' INNER JOIN "public".security_users ON "public".security_users_profiles.users_id = "public".security_users."id"'+
                ' INNER JOIN "public".security_profile ON "public".security_users_profiles.profile_id = "public".security_profile."id"'+
                ' INNER JOIN "public".security_users_profile_permission ON "public".security_users_profiles."id" = "public".security_users_profile_permission.users_profiles_id'+
                ' INNER JOIN "public".auth_permission ON "public".security_users_profile_permission.permission_id = "public".auth_permission."id" WHERE "public".security_users_profile_permission.status = TRUE ')
    Upp = Core.querySql(n)


    dictx={
        "object":Upp,
        "userp":Userp,
        "permx":Permx
    }
    return render(r, "userperfilperm.html",dictx)


def add_user_perfil_perm(r):
    user_per = r.POST.get("user_p")
    user_per = int(user_per)

    perms = r.POST.getlist("perm")

    for d in perms:
        insert_p = users_profile_permission()
        insert_p.users_profiles_id=user_per
        insert_p.permission_id =d
        insert_p.save()
    p = "Se guardo Correctamente"

    return HttpResponse(p)

def index_profilemodul(r):
    sql=connection.cursor()
    sql.execute(' SELECT "public".django_content_type.app_label, "public".django_content_type.model,"public".auth_permission.codename,'+
                ' "public".auth_permission."name", "public".auth_permission.content_type_id,'+
                ' "public".security_profile_permision.permision_id,"public".security_profile_permision.perfil_id,'+
                ' "public".security_profile.description,"public".security_profile.status,'+
                ' "public".security_profile_permision.status FROM '+
                ' "public".security_profile_permision'+
                ' INNER JOIN "public".security_profile ON "public".security_profile_permision.perfil_id = "public".security_profile."id" '+
                ' INNER JOIN "public".auth_permission ON "public".security_profile_permision.permision_id = "public".auth_permission."id"'+
                ' INNER JOIN "public".django_content_type ON "public".auth_permission.content_type_id = "public".django_content_type."id"'+
                ' where "public".security_profile.status = TRUE  AND "public".security_profile_permision.status = TRUE ')
    DataPM=Core.querySql(sql)

    #-----------
    DataProfile=profile.objects.filter(status=True)
    #------
    DataModul=Permission.objects.values("content_type__app_label","content_type__model","codename","content_type_id","name","id")
    DataModul2=ContentType.objects.values().all()
    print(DataModul2)
    ViewData={"modulprofile":DataPM,
              "DataProfile":DataProfile,
              "DataModules":DataModul2}
    return render(r,"profilmodul.html",ViewData)

def json_profilemodul(r):
    id=r.POST.get("id")
    id=int(id)
    DataPerm=Permission.objects.filter(content_type_id=id).values("codename","id","content_type_id","name")
    data=json.dumps(list(DataPerm))
    #print(data)
    return HttpResponse(data)

def add_profilemodul(r):
    perfil=r.POST.get("profile")
    modulo=r.POST.get("modules")
    accion=r.POST.getlist("accion")

    for data in accion:
        per_perm=profile_permision()
        per_perm.perfil_id=perfil
        per_perm.permision_id=data
        per_perm.save()


    return HttpResponse("ok")
