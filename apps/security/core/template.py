from braces.views import LoginRequiredMixin
from django.views.generic import TemplateView
from django.urls import reverse_lazy
from django.core.exceptions import ImproperlyConfigured

# Create your views here.
class indexPsicologia(LoginRequiredMixin,TemplateView):
    login_url = reverse_lazy('error404')
    template_name = 'psicologia/system.html'

    def get_template_names(self):
        """
        Returns a list of template names to be used for the request. Must return
        a list. May not be called if render_to_response is overridden.
        """
        if self.template_name is None:
            raise ImproperlyConfigured(
                "TemplateResponseMixin requires either a definition of "
                "'template_name' or an implementation of 'get_template_names()'")
        else:
            if self.request.GET.get('sys') is None:
                return [self.template_name]
            else:
                return ["psicologia/index.html"]

class Vista(indexPsicologia):
    template_name = ""

    def get_context_data(self, **kwargs):
        context = super(Vista, self).get_context_data(**kwargs)
        context['idprogramacion'] = self.request.GET.get("idprogramacion")
        context['iddetpro'] = self.request.GET.get("iddetpro")
        return context
