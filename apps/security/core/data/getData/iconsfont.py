#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Generate fontawesome icon set in JSON format from website icons.
http://fortawesome.github.io/Font-Awesome/icons/
 
Usage: python icons-font.py
Output: font-awesome.json
{clase: , ...}
 
Author: Anthony Wainer
Date: 2016-01-31
 pip install beautifulsoup4
 pip install html5lib
"""
 
import traceback
import json
import urllib.request as http
from urllib.error import URLError
from bs4 import UnicodeDammit as UD
from bs4 import BeautifulSoup
 

 
def fetch(url):
    if not url:
        return None
 
    try:
        html = None
        response = http.urlopen(url)
    except URLError:
        print('urlopen exception for url %s' % url)
        traceback.print_exc()
    else:
        # Get the byte stream from the http respond.
        bytes_ = response.read()
        # Convert bytes to unicode text.
        html = UD(bytes_).unicode_markup
    return html
 
 
def generate_font_dict():
    html = fetch('http://fortawesome.github.io/Font-Awesome/icons/')
    soup = BeautifulSoup(html,"html5lib")
    s =soup.div.encode("utf-8")
 
    container = soup.find_all('div', class_='fa-hover col-md-3 col-sm-4')
 
    icons = []
 
    for child in container:
       
        clase = child('i')[0]['class'][0] +  ' ' + child('i')[0]['class'][1] 
        icon  = child('i')[0]
        descripcion = child('i')[0]['class'][1].split('fa-')[1]

        txt = str(icon)  + " " +  descripcion 
        icons.append({'id':clase, 'clase' 'text': descripcion})


    return icons
 
 
def main():
    """
   Get dictionary of icon references and dump to JSON.
   """
    icons = generate_font_dict()
    #return icons
    with open('font-awesome.json', 'w') as icon_file:
        json.dump(icons, icon_file)
 
 
if __name__ == '__main__':
    main()