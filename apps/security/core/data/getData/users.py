import json

from django.db import transaction, IntegrityError
from apps.security.models import Users_Core

def user():
    try:
        with transaction.atomic():
            if len(Users_Core.objects.all()) == 0:
                with open('json/users.json') as f:
                    col = json.load(f)

                for i in col:

                    c = Users_Core()
                    c.names             = i["names"]
                    c.dni               = i["dni"]
                    c.email             = i["email"]
                    c.first_name        = i["first_name"]
                    c.last_name         = i["last_name"]
                    c.is_active         = True
                    c.username          = i["username"]
                    c.password_default  = i["password_default"]
                    c.set_password(i["password_default"])
                    c.save()
                    print("se agrego el usuario ")

            else:
                raise ValueError("Hay módulos ingresados, limpia la base de datos")

    except IntegrityError as e:
        error = str(e)
        print(error)

