from .core import edadActual, save_error
from apps.test_psyco.models import Answers, Dimensions, TestPsicologico, Results, Validacion_pf
import sys


def preProcesamiento(dixy, idtest):
    consulta = Answers.objects.values("value_marked", "answer__preguntas__dimensiones",
                                      "answer__alternativas__iddimension"). \
        filter(history=dixy.POST.get('history')).order_by("answer__preguntas__correlativo")
    dim=Dimensions.objects.values().filter(test=idtest)
    test=TestPsicologico.objects.values().filter(id=idtest)
    validation=Validacion_pf.objects.select_related("type_validation").filter(testpsicologico=idtest)
    processing(dixy, consulta, dim, test,validation)


def processing(request, consulta, dimension, test,validation):
    #validation_general(validation)
    save_range_diagnose(request, calculate_score(request, dimension, consulta, test),
                        range_dimention_variable(request, test), test)


def validation_general(validation, **kwargs):

    def validation_formula(validation_type, padre=None):
        for validation_formulate in validation.filter(type_validation_id=validation_type, father=padre):
            try:
                if eval(validation_formulate['validacion']):
                    if validation_formulate['proceso'] != 'continuing':
                        sys.exit()
                    validation_formula(validation_type,validation_formulate['id'])
            except:
                break

    def validation_tipo():
        for validation_type in validation.type_validation_id:
            validation_formula(validation_type)

def calculate_score(request, dimension, consulta, test):
    dimension_formula = []
    dimension_temporal = []
    puntajedimension = {}

    def changing():
        dimension_formula.clear()
        dimension_formula.extend(dimension_temporal)
        dimension_temporal.clear()

    def formulate(formula, pi=None, puntajedimension=None, al=None):
        try:
            return eval(str(formula["formula"]))
        except:
            dimension_temporal.append(formula)
            pass

    def dimention_question():
        pi = For_global(request,Funtions.for_range, range(test.values('questions__id').count()), *consulta)
        changing()
        for formula in dimension_formula:
            puntajedimension[formula["id"]] = formulate(formula, pi.__dict__)

    def dimention_alternatives():
        al = {}
        for numeroAlternativa in test.values('questions__detallealtpreg__id', 'questions__detallealtpreg__puntaje'):
            try:
                al[numeroAlternativa['questions__detallealtpreg__id']] = \
                    int(consulta.values('value_marked')
                        .filter(answer_id=numeroAlternativa['questions__detallealtpreg__id'])[0]['value_marked'])
            except IndexError:
                al[numeroAlternativa['questions__detallealtpreg__id']] = 0
            except:
                save_error(request.user.id,sys.exc_info())
                sys.exit()
        changing()
        for form in dimension_formula:
            puntajedimension[form["id"]] = formulate(form, al=al)

    def dimention_dimention():
        changing()
        for formul in dimension_formula:
            puntajedimension[formul["id"]] = formulate(formul, puntajedimension=puntajedimension)

    funtion = {'question': dimention_question, 'alternatives': dimention_alternatives, 'dimention': dimention_dimention}
    dimension_temporal.extend(dimension)
    for correction in test[0]['level_correction'].split(','):
        funtion[correction]()
    return puntajedimension


def range_dimention_variable(request, test):
    def variable(padre=None):
        date = test.filter(detallevartest__padre=padre)\
            .values("detallevartest__variables__descripcion", "detallevartest__variables__rango"
                    , "detallevartest__variables__categoria__nombre", "detallevartest__id")
        resultado = getattr(Variable_test, date[0]['detallevartest__variables__categoria__nombre'])\
            (variable, request, date)
        try:
            variable(padre=resultado["detallevartest__id"])
        except IndexError:
            return resultado["detallevartest__id"]
        except:
            save_error(request.user.id,sys.exc_info())
            sys.exit()
    return variable()


def save_range_diagnose(request, score, rangedimention, test):
    def save(value, diagonostico=None, time=None):
        guar = Results()
        guar.score = value
        guar.diagnostics_id = diagonostico
        guar.time = 12  # tambien falta determinar
        guar.history_id = int(request.POST.get('history'))
        guar.save()
    print(score)
    for key, value in score.items():
        notrange=True
        try:
            for data in test.filter(dimensions__id=key, dimensions__rangospercentiles__det_variables=rangedimention). \
                    values("dimensions__rangospercentiles__diagnostics_id",
                           "dimensions__rangospercentiles__puntuacion"). \
                    order_by("dimensions__rangospercentiles__puntuacion"):
                if not data['dimensions__rangospercentiles__puntuacion'] <= float(value):
                    notrange=False
                    save(value, diagonostico=diagonostico)
                    break
                else:
                    diagonostico = data["dimensions__rangospercentiles__diagnostics_id"]
            if notrange:
                save(value)
        except:
            #ya esta comprobado
            save_error(request.user.id,sys.exc_info())
            sys.exit()
    pass


class Variable_test():
    __slots__ = 'request', 'date'

    def age(self, request, date):
        edad = edadActual(request.user.birth_date)
        for rangeage in date:
            if float(rangeage['detallevartest__variables__rango'].lower) <= edad and \
                            float(rangeage['detallevartest__variables__rango'].upper) > edad:
                return rangeage
        # falta crear un terminar proceso y mandar un mensaje
        return "La edad esta fuera del rango que permite el test"

    def sex(self, request, date):
        for sex in date:
            if sex['detallevartest__variables__descripcion'] == request.user.sex:
                return sex
        return "No tiene sexo o es un sexo desconocido"

    def generic(self, request, data):
        return data[0]


class Funtions(object):
    def for_range(self, request, foo, *warg):
        try:
            self.__dict__[foo + 1] = (int(warg[foo]["value_marked"]))
        except IndexError:
            self.__dict__[foo + 1] = 0
        except:
            save_error(request.user.id,sys.exc_info())
            sys.exit()


class For_global(Funtions):
    __slots__ = 'funtion', 'parameter'

    def __init__(self, request, function, parameter, *warg, **kwargs):
        self.for_funtion(self, function, parameter, *warg, **kwargs)

    def for_funtion(self, request, funtion, parameter, *warg, **kwargs):
        for foo in parameter:
            funtion(self, request, foo, *warg, **kwargs)
