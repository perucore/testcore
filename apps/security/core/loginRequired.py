from django.contrib.auth.decorators import login_required
from functools import partial
from .filtrourls import urlfilter
from braces.views import LoginRequiredMixin


class LoginRequiredMixinmilton(LoginRequiredMixin):
    login_url = None

    def dispatch(self, request, *args, **kwargs):
        print(request.user.is_authenticated())

        if not request.user.is_authenticated():
            return self.handle_no_permission(request)
        elif not urlfilter(request):
            return self.handle_no_permission(request)
        return super(LoginRequiredMixinmilton, self).dispatch(
            request, *args, **kwargs)