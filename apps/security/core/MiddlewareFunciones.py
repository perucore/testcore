from apps.adm_institucion.models import Institucion
from django.shortcuts import render, HttpResponse,HttpResponseRedirect
from apps.security.views.view_security import index
from django.utils.deprecation import MiddlewareMixin
from django.urls import reverse_lazy
import json

class MiddelwareFunciones(object):
    def __init__(self, get_response=None):
        self.get_response = get_response

    def process_request(self,request):
        url=str(request.get_full_path())
        consult=detail_permission_profile_modules.objects.filter(idprofile__users=request.user.id).\
            values('idpermiso__descripcion')
        url = url.split("?")[0][1:len(url)]
        url = url.split("/")
        if (url[len(url)-1]).find(".")==-1:
            if len(url)>=3:
                for dato in consult:
                    if not dato['idpermiso__descripcion']== url[2]:
                        a=False
                    else:
                        a=True
                        break
                if a:
                    return None
                else:
                    return render(request,'400.html')
        urlexepciones =['reset']
        if not url[0] in urlexepciones:

            if (url[len(url)-1]).find(".")==-1 :

                if len(url)>=3:

                    for dato in consult:

                        if not dato['idpermiso__descripcion']== url[2]:
                            a=False

                        else:
                            a=True
                            break
                    if a:
                        return None
                    else:
                        return render(request,'400.html')


class MiddelwareValidarInstitucion(MiddlewareMixin):

    def process_request(self,request):
        subdom = request.get_host().split('.')[0]  # get subdominio
        b = False  # validate dominio
        if subdom == 'www':
            template_name = "index.html" #template perucore
            p = {}
            return render(request, template_name, p) #return page of perucore.com
        else:
            subdom = '0775'
            ie =  Institucion.__validar_dominio__(subdom) #get data institucion educativa
            if ie:
                request.session['institucion'] = list(ie.values('id','nombre','codigo_modular',
                                                                'direccion','telefono','iniciales','logo'))[0]
                #request.session['institucion']['aulas'] = ''
                b = True
        if b:
            index(request)
        else:
            return HttpResponse("Intitución no Registrada")