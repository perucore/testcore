#s = '"appPagos_det_users_puesto"."id","security_users"."names","security_users"."first_name","security_users"."last_name","security_users"."dni","security_users"."date_birth","security_users"."sex","security_users"."id" AS iduser'
#ex = ['"security_users"."date_birth"']
def searchQ(s, ex, dat):
    ss =s.split(',')

    for c in ss:
        if ' as' in c.lower():
            ss.append(c.lower().split(' as')[0])
            ss.remove(c)

    if ex:
        for i in ex:
            ss.remove(i)

    c = 0
    st = ''
    for i in ss:
        cad = "UPPER(" + i + " :: TEXT) LIKE UPPER ('%"+dat+"%')"
        if c == 0:
            st += cad
        else:
            st += " OR " + cad
        c += 1

    return (st)

