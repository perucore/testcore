from django.db import models,connection
import datetime
from django.utils import timezone


def current_age(born):
    if born is None:
        return None
    today = timezone.now().date()
    return today.year - born.year - ((today.month, today.day) < (born.month, born.day))


def edadActual(fechaNacimiento):
    edad=float(float((datetime.date.today() - fechaNacimiento).days/365))
    return edad


class ManagerMain(models.Manager):
    def get_queryset(self):
        return super(ManagerMain, self).get_queryset().filter(deleted_at__isnull=True)


class TimeStampedModel(models.Model):
    status     = models.BooleanField(default=True)
    created_at = models.DateTimeField(auto_now_add=True,blank=True, null=True)
    updated_at = models.DateTimeField(auto_now=True,blank=True, null=True)
    deleted_at = models.DateTimeField(blank=True, null=True)
    objects    = ManagerMain()
    class Meta:
        abstract = True

def save_error(user, data_error):
    from apps.security.models import SystemError

    guar = SystemError()
    guar.description = data_error[1]
    guar.user = user
    guar.url = "Falta"
    guar.funtion = data_error[2]
    guar.save()

#def save_observation
class Core():

    def querySql(cursor):
        "Return all rows from a cursor as a dict"
        columns = [col[0] for col in cursor.description]
        return [
            dict(zip(columns, row))
            for row in cursor.fetchall()
            ]

    def val_permisos(r,id,sys):


        url = r.get_full_path()
        url = url.split("/")
        #print(sys)
        model_p=url[1]
        model_h=url[2]
        metod=url[3]

        if metod == "" or metod[0] == "?":
            metod="index"


        listPermisos = connection.cursor()

        listaValida=connection.cursor()
        listaValida.execute(
            'SELECT  "public".security_profile.description, '+
            '"public".security_users_profiles."id", '+
            '"public".security_users_profiles.users_id, '+
            '"public".security_users_profiles.profile_id, '+
            '"public".security_users."names", '+
            '"public".auth_permission."name", '+
            '"public".security_users_user_permissions.permission_id,'+
            '"public".security_users_user_permissions.users_id,'+
            '"public".auth_permission.codename,'+
            '"public".django_content_type.app_label,'+
            '"public".django_content_type.model'+
            ' FROM '+
            ' "public".security_users '+
            ' INNER JOIN "public".security_users_profiles ON "public".security_users_profiles.users_id = "public".security_users."id"'+
            ' INNER JOIN "public".security_profile ON "public".security_users_profiles.profile_id = "public".security_profile."id"'+
            ' INNER JOIN "public".security_users_user_permissions ON "public".security_users_user_permissions.users_id = "public".security_users."id"'+
            ' INNER JOIN "public".auth_permission ON "public".security_users_user_permissions.permission_id = "public".auth_permission."id"'+
            ' INNER JOIN "public".django_content_type ON "public".auth_permission.content_type_id = "public".django_content_type."id"'+
            ' WHERE "public".security_users.id='+ str(id) +' and "public".security_users_profiles.profile_id ='+ str(sys) +'  and app_label like \''+str(model_p)+'\' ' +' and model like \''+str(model_h)+'\' '+
            ' and codename =\''+str(metod)+'\'   '

        )

        Lista_val = Core.querySql(listaValida)



        listPermisionFinal=connection.cursor()
        listPermisionFinal.execute(
             'SELECT  "public".django_content_type.app_label, "public".django_content_type.model, "public".auth_permission.codename,'+
             '"public".auth_permission."name", "public".security_users_user_permissions.users_id,'+
              '"public".security_users_user_permissions.permission_id,'+
               '"public".security_users."names",'+
                 '"public".security_users.username,'+
                 '"public".auth_permission.content_type_id,'+
                 '"public".security_users."id",'+
                 '"public".security_users.is_admin'+
             ' FROM '+
                '"public".security_users_user_permissions '+
             ' INNER JOIN "public".security_users ON "public".security_users_user_permissions.users_id = "public".security_users."id" '+
             ' INNER JOIN "public".auth_permission ON "public".security_users_user_permissions.permission_id = "public".auth_permission."id"'+
             'INNER JOIN "public".django_content_type ON "public".auth_permission.content_type_id = "public".django_content_type."id" '+
             ' where users_id ='+ str(id) +'and app_label like \''+str(model_p)+'\' ' +' and model like \''+str(model_h)+'\' ' + ' and codename like \''+str(metod)+'\' '
        )
        ListaFinal=Core.querySql(listPermisionFinal)

        listPermisos.execute(
            'SELECT "public".auth_permission."id", "public".auth_permission."name", "public".auth_permission.content_type_id, "public".auth_permission.codename FROM "public".auth_permission ')

        Listp = Core.querySql(listPermisos)
        #print(Listp)
        var = False
        for name_can in Listp:
            for user_p in Lista_val:
                if name_can['codename'] == user_p['codename']:
                    var = True
                else:
                    var = False
            if var == True:
                break
        return var

    def valida_permisos(r,id,sys):

        url = r.get_full_path()
        url = url.split("/")
        # print(sys)
        model_p = url[1]
        model_h = url[2]
        metod = url[3]

        if metod == "" or metod[0] == "?":
            metod = "index"

        super_user=connection.cursor()
        super_user.execute('SELECT * FROM security_users where security_users.id='+str(id))
        superUser=Core.querySql(super_user)
        for is_super in superUser:
            if(is_super["is_superuser"] == True):
                varx=True
                break

        print(varx)

        acceso=connection.cursor()
        acceso.execute('SELECT   "public".security_profile.description,  "public".django_content_type.app_label,'+
                        ' "public".django_content_type.model, "public".auth_permission."name",'+
                        ' "public".auth_permission.codename   FROM '+
                        ' "public".security_profile_permision '+
                       ' INNER JOIN "public".security_profile ON "public".security_profile_permision.perfil_id = "public".security_profile."id" '+
                       ' INNER JOIN "public".auth_permission ON "public".security_profile_permision.permision_id = "public".auth_permission."id" '+
                       ' INNER JOIN "public".django_content_type ON "public".auth_permission.content_type_id = "public".django_content_type."id" '+
                       ' where  security_profile.id='+ str(sys) +' and security_profile.status = TRUE and security_profile_permision.status = TRUE and django_content_type.app_label = \''+str(model_p)+'\' ' + 'and django_content_type.model= \''+str(model_h)+'\' ' + ' and codename = \''+str(metod)+'\' '+
                        ' UNION '+
                        ' SELECT "public".security_profile.description, "public".django_content_type.app_label,'+
                       ' "public".django_content_type.model, "public".auth_permission."name","public".auth_permission.codename '+
                       ' FROM '+
                       ' "public".security_users_profiles '+
                       ' INNER JOIN "public".security_users ON "public".security_users_profiles.users_id = "public".security_users."id" '+
                       ' INNER JOIN "public".security_profile ON "public".security_users_profiles.profile_id = "public".security_profile."id" '+
                      '  INNER JOIN "public".security_users_profile_permission ON "public".security_users_profiles."id" = "public".security_users_profile_permission.users_profiles_id '+
                       ' INNER JOIN "public".auth_permission ON "public".security_users_profile_permission.permission_id = "public".auth_permission."id" '+
                       ' INNER JOIN "public".django_content_type ON "public".auth_permission.content_type_id = "public".django_content_type."id"  '+
                       ' where security_profile.id='+ str(sys) +' and security_users.id='+ str(id) +' and security_users.status = TRUE  and security_profile.status = TRUE and django_content_type.app_label = \''+str(model_p)+'\' ' +' and django_content_type.model= \''+str(model_h)+'\' ' + ' and codename = \''+str(metod)+'\' ')
        Data=Core.querySql(acceso)
        Cuenta=Data.__len__()

        if(Cuenta == 1):
            var=True
        else:
            var=False

        print(var)
        return var

    def val_vista_perm(r,id,sys):
        url = r.get_full_path()
        url = url.split("/")
        # print(url)
        model_p = url[1]
        model_h = url[2]
        metod = url[3]
        if metod == "":
            metod = "index"

        listPermisionFinal = connection.cursor()
        listPermisionFinal.execute( 'SELECT "public".security_profile.description,"public".django_content_type.app_label,'+
                        ' "public".django_content_type.model,"public".auth_permission."name","public".auth_permission.codename '+
                         ' FROM "public".security_profile_permision  '+
                        ' INNER JOIN "public".security_profile ON "public".security_profile_permision.perfil_id = "public".security_profile."id"  '+
                         ' INNER JOIN "public".auth_permission ON "public".security_profile_permision.permision_id = "public".auth_permission."id"  '+
                         'INNER JOIN "public".django_content_type ON "public".auth_permission.content_type_id = "public".django_content_type."id"  '+
                         ' where security_profile."id"='+ str(sys) +' and security_profile.status = TRUE and security_profile_permision.status=TRUE and django_content_type.app_label = \''+str(model_p)+'\' ' + ' and django_content_type.model= \''+str(model_h)+'\' '+
                         ' UNION  '+
                        ' SELECT "public".security_profile.description,  '+
                        ' "public".django_content_type.app_label,  '+
                        ' "public".django_content_type.model,  '+
                         ' "public".auth_permission."name",  '+
                        ' "public".auth_permission.codename  '+
                        ' FROM "public".security_users_profiles  '+
                        ' INNER JOIN "public".security_users ON "public".security_users_profiles.users_id = "public".security_users."id"  '+
                        ' INNER JOIN "public".security_profile ON "public".security_users_profiles.profile_id = "public".security_profile."id"  '+
                       ' INNER JOIN "public".security_users_profile_permission ON "public".security_users_profiles."id" = "public".security_users_profile_permission.users_profiles_id  '+
                        ' INNER JOIN "public".auth_permission ON "public".security_users_profile_permission.permission_id = "public".auth_permission."id"  '+
                        ' INNER JOIN "public".django_content_type ON "public".auth_permission.content_type_id = "public".django_content_type."id"  '+
                        ' where security_profile."id"='+ str(sys) +' and security_users."id"=' + str(id) + ' and security_users.status = TRUE  and security_profile.status = TRUE and django_content_type.app_label = \'' + str(model_p) + '\' ' + ' and django_content_type.model= \'' + str(model_h) + '\' ')

        ListaFinal = Core.querySql(listPermisionFinal)
        print(listPermisionFinal.query)
        listx=[]
        for can in ListaFinal:
            listx.append(can['codename'])


        return  listx
