# -*- coding: utf-8 -*-
# Generated by Django 1.10.7 on 2017-07-12 07:33
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('security', '0024_auto_20170712_0731'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='Users_Core2',
            new_name='Users',
        ),
    ]
