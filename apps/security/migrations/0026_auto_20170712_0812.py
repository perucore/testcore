# -*- coding: utf-8 -*-
# Generated by Django 1.10.7 on 2017-07-12 08:12
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('security', '0025_auto_20170712_0733'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='users',
            name='groups',
        ),
        migrations.RemoveField(
            model_name='users',
            name='permissions',
        ),
        migrations.RemoveField(
            model_name='users',
            name='profiles',
        ),
        migrations.RemoveField(
            model_name='users',
            name='ubigeo',
        ),
        migrations.RemoveField(
            model_name='users',
            name='user_permissions',
        ),
        migrations.DeleteModel(
            name='Users',
        ),
    ]
