# -*- coding: utf-8 -*-
# Generated by Django 1.10.7 on 2017-05-15 08:54
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('security', '0012_users_institucion'),
    ]

    operations = [
        migrations.RenameField(
            model_name='users',
            old_name='first_name',
            new_name='first_surname',
        ),
        migrations.RenameField(
            model_name='users',
            old_name='last_name',
            new_name='second_surname',
        ),
    ]
