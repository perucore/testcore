from django.apps import AppConfig


class AdmTestConfig(AppConfig):
    name = 'adm_test'
