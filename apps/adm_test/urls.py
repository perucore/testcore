from django.conf.urls import url 
#views.
from .views.v_Program_Test import list_Program_Test,add_Program_Test,update_Program_Test,deleteProgram_Test
from .views.v_Test_Programed import list_Test_Programed,add_Test_Programed,update_Test_Programed,deleteTest_Programed
from .views.v_TestPsicologico import list_TestPsicologico,add_TestPsicologico,update_TestPsicologico,deleteTestPsicologico
from .views.v_History_test_Patient import list_History_test_Patient,add_History_test_Patient,\
  update_History_test_Patient,deleteHistory_test_Patient,list_Patient_json
#.

urlpatterns = [  

  # Program_Test
  url(r'^adm_test/Program_Test/listar/$', list_Program_Test.as_view()),
  url(r'^adm_test/Program_Test/add/$', add_Program_Test.as_view()),
  url(r'^adm_test/Program_Test/update/(?P<pk>\d+)/$', update_Program_Test.as_view()),
  url(r'^adm_test/Program_Test/delete/(?P<id>\d+)/$', deleteProgram_Test),

  # Test_Programed
  url(r'^adm_test/Test_Programed/listar/$', list_Test_Programed.as_view(), name='Test_Programed'),
  url(r'^adm_test/Test_Programed/add/$', add_Test_Programed.as_view()),
  url(r'^adm_test/Test_Programed/update/(?P<pk>\d+)/$', update_Test_Programed.as_view()),
  url(r'^adm_test/Test_Programed/delete/(?P<id>\d+)/$', deleteTest_Programed),

  # TestPsicologico
  url(r'^adm_test/TestPsicologico/listar/$', list_TestPsicologico.as_view()),
  url(r'^adm_test/TestPsicologico/add/$', add_TestPsicologico.as_view(),name='addTestPsicologico'),
  url(r'^adm_test/TestPsicologico/update/(?P<pk>\d+)/$', update_TestPsicologico.as_view()),
  url(r'^adm_test/TestPsicologico/delete/(?P<id>\d+)/$', deleteTestPsicologico),

  # History_test_Patient
  url(r'^adm_test/History_test_Patient/listar/$', list_History_test_Patient.as_view(), name='list_History_test_Patient'),
  url(r'^adm_test/History_test_Patient/add/$', add_History_test_Patient.as_view(), name='add_History_test_Patient'),
  url(r'^adm_test/History_test_Patient/update/(?P<pk>\d+)/$', update_History_test_Patient.as_view(), name='update_History_test_Patient'),
  url(r'^adm_test/History_test_Patient/delete/(?P<id>\d+)/$', deleteHistory_test_Patient, name='delete_History_test_Patient'),

  url(r'^adm_test/History_test_Patient/list_json/$', list_Patient_json.as_view()),
]