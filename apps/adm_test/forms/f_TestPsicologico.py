from django import forms
from ..models import TestPsicologico

class TestPsicologicoForm(forms.ModelForm):
  class Meta():
    model = TestPsicologico
    fields = '__all__'
    exclude = ('status', 'deleted_at')