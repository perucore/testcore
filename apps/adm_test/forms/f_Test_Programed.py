from django import forms
from ..models import Test_Programed
from apps.test_psyco.models import TestPsicologico

class Test_ProgramedForm(forms.ModelForm):
  def __init__(self, *args, **kwargs):
    super(Test_ProgramedForm, self).__init__(*args, **kwargs)
    #self.fields['test'] = forms.ChoiceField(choices=)

  class Meta():
    model = Test_Programed
    fields = '__all__'
    exclude = ('status', 'deleted_at')
    widgets ={
      'date' : forms.DateInput(format=('%Y-%m-%d'), attrs={'type': 'date','placeholder': 'dd/mm/aaaa'}),
      'time': forms.DateInput(format=('%H:%m'), attrs={'type': 'time', 'placeholder': 'HH:MM'}),
    }
    labels ={
      'date' : 'Fecha del test',
      'time': 'Hora del test',
      'program_test': 'Programa',
      'patient': 'Pacientes'
    }

