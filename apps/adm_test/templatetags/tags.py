from apps.adm_test.models import History_test_Patient
from django import template
register = template.Library()

def get_patient(pa):
    li = [str(s.patient_id) for s in History_test_Patient.__get__patient__('',{'test_programed_id': pa})]
    print(li)
    return li

@register.simple_tag
def check_student(st,pa):
    p = get_patient(pa)
    if st in p:
        return "checked"
    else:
        ""

def check_student2(st,pa):
    p = get_patient(pa)
    return st in p


register.filter(check_student2)