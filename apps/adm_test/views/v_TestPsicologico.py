from django.contrib.auth.decorators import login_required
from django.shortcuts import HttpResponse

from apps.security.core.crud import ListBase, SaveBase, UpdateBase
from ..forms.f_TestPsicologico import TestPsicologicoForm
from ..models import TestPsicologico


class list_TestPsicologico(ListBase):
  template_name = 'TestPsicologico/list_TestPsicologico.html'
  queryset = TestPsicologico.objects.filter(status=True)

class add_TestPsicologico(SaveBase):
  form_class = TestPsicologicoForm
  template_name = 'TestPsicologico/frm_TestPsicologico.html'

class update_TestPsicologico(UpdateBase):
  form_class = TestPsicologicoForm
  template_name = 'TestPsicologico/frm_TestPsicologico.html'
  model = TestPsicologico

@login_required(login_url='login/')
def deleteTestPsicologico(em, id):
  dp = TestPsicologico.objects.get(pk=id)
  dp.status = False
  dp.save()
  return HttpResponse('DATOS ELIMINADOS') 