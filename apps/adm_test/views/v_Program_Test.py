from apps.security.core.join_list import validate_dict
from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect
from django.shortcuts import HttpResponse

from apps.security.core.crud import ListBase, SaveBase, UpdateBase
from ..forms.f_Program_Test import Program_TestForm
from ..forms.f_Test_Programed import Test_ProgramedForm
from ..models import Program_Test, Test_Programed


class list_Program_Test(ListBase):
  template_name = 'Program_Test/list_Program_Test.html'

  def l_queryset(self):
    queryset = Program_Test.objects.filter(status=True)
    return queryset


class add_Program_Test(SaveBase):
  form_class = Program_TestForm
  form_class2 = Test_ProgramedForm
  template_name = 'Program_Test/frm_Program_Test.html'

  def save_test_pogramed(self,c,ss):
    nl = validate_dict(c)
    nl['program_test'] = ss.id
    form = self.form_class2(nl)
    if form.is_valid():
      fff = form.save(commit=False)
      fff.save()
    else:
      return self.form_invalid(form)

  def form_valid(self, form):
    ss = form.save(commit=False)
    ss.ie_id = int(self.request.session["institucion"]["id"])
    ss.programmer_id = self.request.user.id
    ss.save()

    if self.request.POST.get('m2m') is not None:
      lis = eval(self.request.POST.get('m2m'))
      if type(lis) == "<class 'list'>":
        for c in lis:
          self.save_test_pogramed(c, ss)
      else:
        self.save_test_pogramed(lis, ss)

    return HttpResponseRedirect(self.get_success_url())

class update_Program_Test(UpdateBase):
  form_class = Program_TestForm
  form_class2 = Test_ProgramedForm
  template_name = 'Program_Test/frm_Program_Test.html'
  model = Program_Test
  model2 = Test_Programed

  def l_queryset(self,p):
    q = self.model2.objects.filter(program_test_id=self.kwargs["pk"])
    return q

  def form_valid(self, form):
    ss = form.save(commit=False)
    ss.save()

    if self.request.POST.get('m2m') is not None:
        q = self.l_queryset('')
        idmf = [a.id for a in q]

        mf = []
        for c in eval(self.request.POST.get('m2m')):
          y = validate_dict(c)
          y['program_test'] = ss.id
          if "id" in y:
            mf.append(int(y["id"]))
            if int(y["id"]) in idmf:
              self.model2.objects.filter(pk=y["id"]).update(**y)
          else:
            form = self.form_class2(y)
            if form.is_valid():
              fff = form.save(commit=False)
              fff.save()
            else:
              return self.form_invalid(form)

        for a in idmf:
          if not (a in mf):
            self.model2.objects.get(pk=a).delete()

        return HttpResponseRedirect(self.get_success_url())

@login_required(login_url='login/')
def deleteProgram_Test(em, id):
  dp = Program_Test.objects.get(pk=id)
  dp.status = False
  dp.save()
  return HttpResponse('DATOS ELIMINADOS') 