

$( document ).ready(function() {

    setTimeout(function(){ M.toast({html:'Bienvenido a TestCore!'}, 2000) }, 2000);

    // CounterUp Plugin
    $('.counter').each(function () {
        $(this).prop('Counter',0).animate({
            Counter: $(this).text()
        }, {
            duration: 3500,
            easing: 'swing',
            step: function (now) {
                $(this).text(Math.ceil(now));
                $(this).text($(this).text().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"));
            }
        });
    });


});

$(window).hashchange(function() {
    cargar_url();

});

function cargar_url() {
    str = window.location.hash;

    if (str.length >= 1){
        str = str.substring(1, str.length);
        traemedatos(str);
        activarModulo(url);
        //$("#"+str).addClass('sfActive');
        //f = $("#"+str).attr('father');
        //$("#"+f).css('style','block');
    }else{
        traemedatos('dashboard/');
    }
}


/**
 * Created by peruhardsoft on 15/09/16.
 */
  /*Preloader*/
$(window).load(function(){
    cargar_url();
    //$( "#tab-p" ).sortable();
});



function breadCrum(u) {
    breadcrum = u.split("/");
    tam = breadcrum.length;
    html = '';
    url = breadcrum[0] + "/";
    for (i = 1; i< tam; i++){
      if (i==3 || breadcrum[i] == ""   ){
           break;
      }
      url += breadcrum[i] + "/";
      html += '<li> <a href="#'+url+'"> '+breadcrum[i]+'</a> / </li>';
    }
    html = '<li><a href="#"><i class="fa fa-home"></i> Inicio</a> </li> /' +'<li><a> '+breadcrum[0]+'</a> </li>/' + html;
    $("#dashboard").html(html);
    return true;
}

function traemedatos(u){
     breadCrum(u);
    if (addtab(u)) {
        tab = u.split("/");
        id = tab[0]+'-'+tab[1]+'-'+tab[2];

        $.ajax({
            type: 'GET',
            url: u,
            //data: {'sys':s},
            success: function (d) {
                ht = "<div class='resultados' id='r-"+id+"'>"+d+"</div>";
                //$('#resultados').append(ht);
                $('#resultados').empty().html(ht);
            },
            error: function (xhr, status, error) {
                ht = "<div class='resultados' id='r-"+id+"'>"+xhr.responseText+"</div>";
                //$('#resultados').append(ht);
                $('#resultados').empty().html(ht);
            },
            beforeSend: function (xhr, status) {
                // TODO: show spinner
               // $("#algo").attr('style', 'position: absolute;height: 100%; width: 100%; z-index: 100;');
                $('#spinner').fadeIn(400);
            },
            complete: function () {
                // TODO: hide spinner
                //$("#algo").attr('style', '');
                $('#spinner').fadeOut(400, "linear");

            }
        });
        //$(".resultados").css('display','NONE');
        //$("#r-"+id).css('display','block');
    }
}
function addtab(u){
    ur = [];
    $(".tab-p").each(function () {
        id = $(this).attr('id');
        ur.push(id);
    });
    tab = u.split("/");

    if (tab.length>2){
        tb = tab[2].split("?")[0];
        pes = tab[1]+'/'+tb;
    }else{
        tb='';
        pes = tab[0];
    }

    id = tab[0]+'-'+tab[1]+'-'+tb;
    if ($.inArray(id,ur) >= 0){
        $(".tab-p").removeClass('tactive');
        $("#"+id).addClass('tactive');
        //$(".resultados").css('display','NONE');
        //$("#r-"+id).css('display','block');
        //return false;
        return true;
    }

    html ='<div class="tab-p tactive"  id="'+id+'" >';
    html += '<a style="margin-right: 10px" ondblclick="location.hash = \'#'+u+'\';fullscreen(document.getElementById(\'resultados\'))" href="#'+u+'" onclick="tabp(this,\''+u+'\')">'+pes+'</a>';
    html += '<i class="material-icons close" onclick="closet(this)">close</i>';
    html += '<i class="material-icons tiny" href="#'+u+'" onclick="tabp(this,\''+u+'\')">settings_backup_restore</i>';
    html += '<i class="material-icons tiny" onclick="location.hash = \'#'+u+'\';fullscreen(document.getElementById(\'resultados\'))">open_with</i>';
    html += '</div>';
    $(".tab-p").removeClass('tactive');
    $("#tab-p").append(html);

    return true;

}
    function tabp(t,u) {
       $(".tab-p").removeClass('tactive');
       $(t).closest('div').addClass('tactive');
       traemedatos(u);
       //$(".resultados").css('display','NONE');
        //$("#r-"+$(t).attr('id')).css('display','block');
    }
    function closet(t) {
       $(t).closest('div').remove();
       if ($(t).hasClass('tactive')){
            $("#resultados").empty();
       }


       ur=[];
        $(".tab-p a").each(function () {
            id = $(this).attr('href');
            ur.push(id);
        });

        if (ur.length>0){
            window.location.hash = ur[ur.length-1];
        }else{
            window.location.hash = "#dashboard/";
        }
       //$("#r-"+$(t).closest('div').attr('id')).remove();
    }

    function noti(type,msg){
        Lobibox.notify(type, {
        size: 'mini',
        rounded: true,
        delayIndicator: true,
        msg: msg, delay: 2500,
    });
    }

function clickmenu(t,u) {

    $('a.active-page').each(function (a,i) {
        $(i).removeClass();
    });
    $(t).addClass('open');
    if (window.location.hash ) {
        if (str == u) {
            return traemedatos(u);
        }

    }

    window.location.href = "#" + u;
}

function activarModulo(url){
    tab = url.split("/");
    id = tab[0]+'-'+tab[1]+'-'+tab[2];

    if(tab[0] == "security"){
        $("#seguridad1").addClass('active');
        $("#seguridad2").addClass('active');
        $("#seguridad3").css('display','block');
        if(tab[1] == "profile"){
            $("#perfil_usuario").addClass('active-page');
        }else if(tab[1] == "modules"){
            $("#modulos").addClass('active-page');
        }else if(tab[1] == "usuarios"){
            $("#usuarios").addClass('active-page');
        }else if(tab[1] == "permission"){
            $("#permisos").addClass('active-page');
        }
    }else{
        $('#'+tab[0]+"1").addClass('active');
        $('#'+tab[0]+"2").addClass('active');
        $('#'+tab[0]+"3").css('display','block');
        $('#'+tab[1]).addClass('active-page');
    }

}
function atras() {
    window.history.back();
}

$(function() {
  $.fn.required = function() {
    if ( $(this).val() == '' || $(this).val() == 0 ) {
        $(this).css('border','solid 2px red');
        $('#msg').html('<label class="lbl_msg">Debes llenar todos los campos necesarios</label>');
        $(this).focus();
        return false;
    }else {
        $(this).css('border','solid 1px #ccc');
        $('#msg').html('');
        return true;
    }
  };
});

 fullscreen = function(e){
      if (e.webkitRequestFullScreen) {
        e.webkitRequestFullScreen();
      } else if(e.mozRequestFullScreen) {
        e.mozRequestFullScreen();
      }
  }

function check_js(url) {
  len = $('script').filter(function () {
    return ($(this).attr('src') == url);
  }).length;
  if (len==0){
      return false;
  }else{
      return true;
  }

}

