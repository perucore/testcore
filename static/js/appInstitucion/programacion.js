$(function(){
    //para que se muestre los tag select
    $("#institucion").material_select();
    $("#area").material_select();
    $("#grupo").material_select();
    $("#test_id").material_select();

    $("#guardar").click(function(){
        if(validar_form("#form_programacion")){
            var permisoclick=$(this);
            if(!permisoclick.data('permiso')){
                $.ajax({
                    method: "GET",
                     url: 'test/programacion/guardarprogramacion',
                     data: $("#form_programacion").serialize(),
                     beforeSend: function(){ permisoclick.data('permiso', true);},
                     success: function(data){noti('success',data)},
                     error: function(){noti("error","Problemas al guardar")},
                     complete: function(){ permisoclick.data('permiso', false);}
                });
            }
        }
    });

    $("#institucion").change(function(){
        $.get("test/programacion/extraerareagrupo", {'institucion':$(this).val()},function(dato){
            var area='';
            if(dato.area.length>0){
                $.each(dato.area,function(key,value){
                    area+='<option value="'+value['id']+'">'+ value['nombre']+'</option>'
                });
            }
            var grupo='';
            if(dato.grupo.length>0){
                $.each(dato.grupo,function(key,value){
                    grupo+='<option value="'+value['id']+'">'+ value['nombre']+'</option>'
                });
            }
            $("#area").html(area);
            $("#grupo").html(grupo);
            $("#area").material_select();
            $("#grupo").material_select();
        },'json')
    });

});