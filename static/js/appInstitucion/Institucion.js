$(function(){
    //para que se muestre los tag select
    $("#inst_padre").material_select();

    $("#guardar").click(function(){
        if(validar_form("#form_institucion")){
            var permisoclick=$(this);
            if(!permisoclick.data('permiso')){
                $.ajax({
                    method: "GET",
                     url: 'test/institucion/guardarinstitucion',
                     data: $("#form_institucion").serialize(),
                     beforeSend: function(){ permisoclick.data('permiso', true);},
                     success: function(data){noti('success',data)},
                     error: function(){noti("error","Problemas al guardar")},
                     complete: function(){ permisoclick.data('permiso', false);}
                });
            }
        }
    })

});