$(function(){
    $("#agregarInstitucion").click(function(){
        var permisoclick=$(this);
        if(!permisoclick.data('permiso')){
            $.ajax({
                method: "GET",
                 url: 'test/institucion/agregarinstitucion/',
                 beforeSend: function(){ permisoclick.data('permiso', true);},
                 success: function(data){
                     $("#resultados").html(data)
                 },
                 error: function(){ },
                 complete: function(){ permisoclick.data('permiso', false);}
            });
        }
    });
})

function editarInstitucion(id){
    var permisoclick=$(this);
        if(!permisoclick.data('permiso')){
            $.ajax({
                method: "GET",
                url: 'test/institucion/editarinstitucion',
                data:{'id':id},
                beforeSend: function(){ permisoclick.data('permiso', true);},
                success: function(data){
                    $("#resultados").html(data)
                },
                error: function(){ },
                complete: function(){ permisoclick.data('permiso', false);}
            });
        }
}