$(function(){
    //para que se muestre los tag select
    $("#institucion_id_id").material_select();
    $("#grupo_padre").material_select();

   $("#guardar").click(function(){
       if(validar_form("#form_grupo")){
           var permisoclick=$(this);
            if(!permisoclick.data('permiso')){
                $.ajax({
                    method: "GET",
                     url: 'test/grupo/guardargrupo',
                     data: $("#form_grupo").serialize(),
                     beforeSend: function(){permisoclick.data('permiso', true);},
                     success: function(data){noti("success",data)},
                     error: function(){noti("error","Problemas al guardar")},
                     complete: function(){ permisoclick.data('permiso', false);}
                });
            }
       }
   })
});