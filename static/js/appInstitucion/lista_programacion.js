$(function(){
    $("#agregarProgramacion").click(function(){
        var permisoclick=$(this);
        if(!permisoclick.data('permiso')){
            $.ajax({
                method: "GET",
                 url: 'test/programacion/agregarProgramacion/',
                 beforeSend: function(){ permisoclick.data('permiso', true);},
                 success: function(data){
                     $("#resultados").html(data)
                 },
                 error: function(){ },
                 complete: function(){ permisoclick.data('permiso', false);}
            });
        }
    })
});
    //regireccionar
    function editarProgramacion(id){
        var permisoclick=$(this);
        if(!permisoclick.data('permiso')){
            $.ajax({
                method: "GET",
                 url: 'test/programacion/editarprogramacion/',
                 data: {'id':id},
                 beforeSend: function(){ permisoclick.data('permiso', true);},
                 success: function(data){noti('success',data)},
                 error: function(){noti("error","Problemas al querer editar")},
                 complete: function(){ permisoclick.data('permiso', false);}
            });
        }
    }

    function anularProgramacion(id){
        var permisoclick=$(this);
        if(!permisoclick.data('permiso')){
            $.ajax({
                method: "GET",
                 url: 'test/programacion/formularioProgramacion/',
                 data: {'id':id},
                 beforeSend: function(){ permisoclick.data('permiso', true);},
                 success: function(data){noti('success',data)},
                 error: function(){noti("error","Problemas al querer editar")},
                 complete: function(){ permisoclick.data('permiso', false);}
            });
        }
    }