function DeleteMod(id) {
    $.get('security/modules/delete/'+id+'/', function(data) {
        JSONMOD();
        noti("error",data);
    });
}
function UpdateMod(id) {
    $.getJSON('security/modules/edit/'+id+'/?type=json', function(data) {
       $.each(data,function(key,valor) {
         //  alert(valor.icon.id);
           m =  Modal('ided',"EDITAR MÓDULO",'',
               "<input type='button' value ='registrar' onclick='updateSaveMod(this,"+valor.id+")' class='btn btn-primary'>",
               "");
           $('#myModal').html(m);
           $('#ided').modal();
           $('#ided').modal('open');
           t = _.template($("#tFormMod").html());
           $(".modal-body").empty().html(t({data:data,id:id}));
           se('.moduleB');
           //captura evento de selecion en select2
           $(".moduleB").on("select2:select",function(e){ $('#formM .select2-selection .select2-selection__rendered').empty().append( e.params.data.clase); });
           $('#formM .select2-selection .select2-selection__rendered').append(valor.icon__clase);
           i =1;
           $('span[dir="ltr"]').each(function() {
             i++;
             if (i == 4){
             $(this).remove();
            }
           
         });
       });
    });
}

function updateSaveMod(t,id){
  f = $("#formM").serialize();
  $(t).prop("disabled",true);
  $.post('security/modules/edit/'+id+'/', f+'&mod=1', function(data) {
        JSONMOD();
        noti('success',data)
  }).fail(function(resp) {
        errornormal(resp,"formM");
        noti('error','datos sin llenar')
  }).done(function(a){
        cerrarModal('ided');
  });
}

//--------------------------------------
//------registrar module-----------------
$("#registermodule").click(function() {
    s = $('#Formmodule').serialize();
    $.post('security/modules/vista_modulo/?type=json', s, function(data) {
       JSONMOD();
    }).fail(function(resp) {
        errorf(resp);
    });
});
//----------------------------------------
//----------------------------------------
JSONMOD();
function JSONMOD(){
    $.getJSON('security/modules/vista_modulo/?type=json&id=', function(data) {
        t = _.template($("#tmodule").html());
        $("#m1").html(t({data:data}));
    });
}

//---------------------
se(".module");
//---------------------
function se(clase){
   $(clase).select2({
    ajax: {
      //url: "https://api.github.com/search/repositories",
      url: "security/modules/fonts/",
      dataType: 'json',
      allowClear: true,
      // delay: 0,
      data: function (params) {
        return {
          q: params.term, // search term
          //page: params.page
        };
      },
      processResults: function (data, params) {
      //params.page = params.page || 1;
          return {
          results: data,
          /*pagination: {
            more: (params * 30) < data.total_count
          }*/
        };
      },
      cache: true
    },
    escapeMarkup: function (markup) { return markup; },
    templateResult: function (repo) {
     if (!repo.clase) { return repo.description; }
        var $repo = $('<span><i class="'+repo.clase+'"></i>  ---->    ' + repo.description+ '</span>');
        return $repo;
    },
    templateSelection: function (repo) {
      return repo.clase;
    },

  });
 

}
