//para subir archivos con ajax
function remover_error() {
    $('.errorlist').remove();
}

function guardarF(url,form,imp ){
    formData = new FormData($('#form'+form)[0]);
    //formData.append('id', $(".guar").attr('id'));
    $.ajax({
        url: url,  
        type: 'POST',
        data: formData,
        //necesario para subir archivos via ajax
        cache: false,
        contentType: false,
        processData: false,

        success: function(data){
            //function json
        }

    }).done(function() {
    //función para imprimir 

    }).fail(function(resp) {
        remover_error();
        var errors = JSON.parse(resp.responseText);
        for (e in errors) {     
            var id = '#id_' + e;
            $(id).after(errors[e]);
        }
    });
}

function printJ(url, imp){
    $.getJSON(url, function(data){
        html = '';
        $.each(data, function(i, v) {
                html += '<tr><td>'+v.descripcion+'</td></tr>';
            });
            $(imp).html(html);
        });
}

