$("form").submit(function (e) {
     $("button[type=submit]").prop("disabled",true);
     e.preventDefault();
    w=$(this).serialize();
    $.ajax({
    url:"/login/",
    type: 'POST',
    data: w,
    success: function () {
        unblock_form();
        $('#msmlogin').removeClass('hide');
        $('#msmlogin').addClass('alert teal');
        $("#msmlogin").html("Sesión Iniciada, Redirigiendo...");
        location.reload();
    },
    error: function (resp) {
        unblock_form();
        var errors = JSON.parse(resp.responseText);
        for (ee in errors) {
            if (ee == '__all__'){
                $('#msmlogin').removeClass('hide');
                $('#msmlogin').addClass('alert');
                $('#msmlogin').empty().prepend(errors[ee]);
                $("button[type=submit]").prop("disabled",true);
            }
            var id = '#id_' + ee;
            $(id).after(errors[ee]);
        }
    },
            beforeSend: function (xhr, status) {
                // TODO: show spinner
               // $("#algo").attr('style', 'position: absolute;height: 100%; width: 100%; z-index: 100;');
               // $('#spinner').fadeIn(400);
                for (i=1; i<=100; i++){
                    $("#pace-bar").css('width', i + '%');
                }

            },


    });
});
$("input").keydown(function () {
    $("button[type=submit]").prop("disabled",false);
    $("#pace-bar").css('width', 1 + '%');
    $("#msmlogin").empty();
});
function unblock_form() {
   $('.errorlist').remove();
}


function  contacto(t){
    s = $(t).serialize();
    $.post("contacto/",s).done(function(d){
        unblock_form();
        //imprimir mensaje
    }).fail(function(resp) {
        unblock_form();
        var errors = JSON.parse(resp.responseText);
        console.log(resp);
        for (e in errors) {
            if (e == '__all__'){
                $('#msmlogin').html(errors[e]);
            }
            var id = '#id_' + e;
            $(id).after(errors[e]);
        }
    });
    return false;
}



