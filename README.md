﻿## TestCore
Es una plataforma web en la nube, que automatiza los procesos de la Evaluación Psicológica

### Versión
V2.3

### Tecnologías

* [Django2.1] - Django hace que sea más fácil de construir mejores aplicaciones web más rápido y con menos código.

### Installation

$ git clone
$ pip install -r requirements.txt
--settings=config.settings

### Configurar Base de Datos `testCore/config/db_example.py`

copiar db_example.py a local.py
```sh
$  cp testCore/config/db_example.py testCore/config/db.py
```
<br>
DATABASES = {
    'default': {
       'ENGINE'  : 'django.db.backends.postgresql_psycopg2',
       'NAME'    : 'testCore',
       'USER'    : 'testCore',
       'PASSWORD': 'testCore',
       'HOST'    : 'localhost',
       'PORT'    : '5432',
    }
}
```

### Migraciones [warning]

```sh
$  python manage.py makemigrations --settings=config.settings
$  python manage.py migrate --settings=config.settings
```

### Crear Super Usuario
```sh
$  python manage.py createsuperuser
```

### agregar data inicial
```sh
$  python start.py
```

### Iniciar
```sh
$ python manage.py runserver testcore.herokuapp.com
```



### Desarrollo
 celery worker -A config.celery -l info



